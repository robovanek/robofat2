#include <robofat2/utilities/strings.hpp>

void
robofat2::utilities::rtrim(std::string &str) {
    size_t end = str.find_last_not_of(" \t\r\n");
    if (end + 1 == str.length())
        return;
    if (end == std::string::npos)
        str.erase();
    else
        str.erase(end + 1);
}

void
robofat2::utilities::ltrim(std::string &str) {
    size_t start = str.find_first_not_of(" \t\r\n");
    if (start == 0)
        return;
    if (start == std::string::npos)
        str.erase();
    else
        str.erase(0, start);
}

void
robofat2::utilities::trim(std::string &str) {
    ltrim(str);
    rtrim(str);
}
