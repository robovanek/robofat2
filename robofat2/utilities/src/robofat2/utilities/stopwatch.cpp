#include <robofat2/utilities/stopwatch.hpp>

using namespace robofat2::utilities;

stopwatch::stopwatch()
        : m_mark(duration(0)), m_empty(true) {}

void stopwatch::reset_mark() {
    m_empty = true;
}

bool stopwatch::is_mark_empty() const {
    return m_empty;
}

stopwatch::duration stopwatch::get_elapsed_since_mark() const {
    if (m_empty) {
        return duration(0);
    } else {
        return clock::now() - m_mark;
    }
}

void stopwatch::set_mark_to_now() {
    m_mark = clock::now();
    m_empty = false;
}

stopwatch::time_point stopwatch::get_mark() const {
    return m_mark;
}

void stopwatch::set_mark(time_point point) {
    m_mark = point;
    m_empty = false;
}
