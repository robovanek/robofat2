#include <gtest/gtest.h>
#include <robofat2/utilities/strings.hpp>

using namespace robofat2::utilities;

std::string ltrim_value(std::string input) {
    ltrim(input);
    return input;
}

std::string rtrim_value(std::string input) {
    rtrim(input);
    return input;
}

std::string trim_value(std::string input) {
    trim(input);
    return input;
}

TEST(strings, ltrim) {
    EXPECT_EQ(ltrim_value("\t \naa  \t a \t\n\r"), "aa  \t a \t\n\r");
}

TEST(strings, rtrim) {
    EXPECT_EQ(rtrim_value("\t \naa  \t a \t\n\r"), "\t \naa  \t a");
}

TEST(strings, bothtrim) {
    EXPECT_EQ(trim_value("\t \naa  \t a \t\n\r"), "aa  \t a");
}
