#include <gtest/gtest.h>
#include <robofat2/utilities/stopwatch.hpp>
#include <thread>

using namespace robofat2::utilities;
using namespace std::chrono;

TEST(stopwatch, init_empty) {
    stopwatch timer;
    EXPECT_TRUE(timer.is_mark_empty());
}

TEST(stopwatch, auto_set) {
    stopwatch timer;

    timer.set_mark_to_now();
    EXPECT_FALSE(timer.is_mark_empty());
}

TEST(stopwatch, manual_set) {
    stopwatch timer;

    timer.set_mark(steady_clock::now());
    EXPECT_FALSE(timer.is_mark_empty());
}

TEST(stopwatch, mark_get_set) {
    stopwatch timer;
    auto start = steady_clock::now();
    timer.set_mark(start);
    EXPECT_EQ(timer.get_mark(), start);
}

TEST(stopwatch, elapsed_manual) {
    milliseconds sleep(50);

    stopwatch timer;
    auto start = steady_clock::now();
    timer.set_mark(start);

    std::this_thread::sleep_for(sleep);

    EXPECT_GE(timer.get_elapsed_since_mark(), sleep);
    EXPECT_TRUE(timer.has_elapsed_since_mark(sleep));
    EXPECT_FALSE(timer.has_elapsed_since_mark(milliseconds(1000)));
    EXPECT_GE(steady_clock::now() - timer.get_mark(), sleep);
}

TEST(stopwatch, elapsed_auto) {
    milliseconds sleep(50);

    stopwatch timer;
    timer.set_mark_to_now();

    std::this_thread::sleep_for(sleep);

    EXPECT_GE(timer.get_elapsed_since_mark(), sleep);
    EXPECT_TRUE(timer.has_elapsed_since_mark(sleep));
    EXPECT_FALSE(timer.has_elapsed_since_mark(milliseconds(1000)));
    EXPECT_GE(steady_clock::now() - timer.get_mark(), sleep);
}

TEST(stopwatch, reset) {
    stopwatch timer;

    timer.set_mark_to_now();
    timer.reset_mark();
    EXPECT_TRUE(timer.is_mark_empty());
}

TEST(stopwatch, uninitialized_elapsed) {
    stopwatch timer;
    timer.reset_mark();

    EXPECT_TRUE(timer.has_elapsed_since_mark(milliseconds(0)));
    EXPECT_EQ(timer.get_elapsed_since_mark(), nanoseconds(0));
}
