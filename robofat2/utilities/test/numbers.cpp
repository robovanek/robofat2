#include <gtest/gtest.h>
#include <robofat2/utilities/numbers.hpp>

using namespace robofat2::utilities;

TEST(numbers, inversion_sign) {
    EXPECT_EQ(+1, inversion_sign(false));
    EXPECT_EQ(-1, inversion_sign(true));
}

TEST(numbers, inverted) {
    EXPECT_FALSE(inverted(1));
    EXPECT_FALSE(inverted(0));
    EXPECT_TRUE(inverted(-1));
}

TEST(numbers, signum) {
    EXPECT_EQ(+1, signum(+2));
    EXPECT_EQ(+1, signum(+1));
    EXPECT_EQ(0, signum(0));
    EXPECT_EQ(-1, signum(-1));
    EXPECT_EQ(-1, signum(-2));
}

TEST(numbers, limit) {
    EXPECT_EQ(3, limit(10, 1, 3));
    EXPECT_EQ(3, limit(3, 1, 3));
    EXPECT_EQ(2, limit(2, 1, 3));
    EXPECT_EQ(1, limit(-5, 1, 3));
    EXPECT_EQ(1, limit(-5, 1, 3));
    EXPECT_EQ(-5, limit(-10, -5, +5));
    EXPECT_EQ(+5, limit(+10, -5, +5));
}

TEST(numbers, is_bounded) {
    EXPECT_FALSE(is_bounded(10, 1, 3));
    EXPECT_TRUE(is_bounded(3, 1, 3));
    EXPECT_TRUE(is_bounded(2, 1, 3));
    EXPECT_FALSE(is_bounded(-5, 1, 3));
    EXPECT_FALSE(is_bounded(-5, 1, 3));
    EXPECT_FALSE(is_bounded(-10, -5, +5));
    EXPECT_FALSE(is_bounded(+10, -5, +5));
}

TEST(numbers, roundToInt) {
    EXPECT_EQ( 1, roundToInt(1.0f));
    EXPECT_EQ( 1, roundToInt(1.49f));
    EXPECT_EQ( 2, roundToInt(1.50f));
    EXPECT_EQ( -1, roundToInt(-0.50f));
    EXPECT_EQ( 0, roundToInt(-0.49f));
    EXPECT_EQ(-10000, roundToInt(-10000.0f));
}

TEST(numbers, pi) {
    EXPECT_EQ(314159, roundToInt(PI_F * 100000.0f));
    EXPECT_EQ(314159, roundToInt(PI * 100000.0));
}
