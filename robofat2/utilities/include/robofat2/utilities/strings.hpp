#ifndef ROBOFAT2_UTILITIES_STRINGS_HPP
#define ROBOFAT2_UTILITIES_STRINGS_HPP

#include <string>

namespace robofat2 {
    namespace utilities {

        /**
         * \brief Left-trim the string.
         *
         * @param str   Read-write string.
         */
        extern void ltrim(std::string &str);

        /**
         * \brief Right-trim the string.
         *
         * @param str   Read-write string.
         */
        extern void rtrim(std::string &str);

        /**
         * \brief Trim the string.
         *
         * @param str   Read-write string.
         */
        extern void trim(std::string &str);
    }
}

#endif //ROBOFAT2_UTILITIES_STRINGS_HPP
