#ifndef ROBOFAT2_UTILITIES_NUMBERS_HPP
#define ROBOFAT2_UTILITIES_NUMBERS_HPP

#include <cmath>

namespace robofat2 {
    namespace utilities {
        /**
         * \brief Get inversion sign from inversion bool
         *
         * @param invert   If the output should be inverted (i.e. negative)
         * @return         {@code 1} if {@code invert == false}, {@code -1} if {@code invert == true}
         */
        inline int inversion_sign(const bool invert) {
            return invert ? -1 : 1;
        }

        /**
         * \brief Reverse function to inversion_sign
         * @param arg   1 or -1
         * @return      {@code true} if {@code arg < 0}, false otherwise
         */
        inline bool inverted(const int arg) {
            return arg < 0;
        }

        /**
         * \brief Signum function of the specified value
         *
         * @param val   Argument to signum
         * @return      -1 if negative, 0 if 0, 1 if positive
         */
        template<typename number>
        inline number signum(number val) {
            return (0 < val) - (val < 0);
        }

        /**
         * \brief Clip the value to specified bounds
         *
         * @param value   Value to limit
         * @param min     Lower limit
         * @param max     Upper limit
         * @return        Clipped value
         */
        template<typename number>
        inline number limit(number value, number min, number max) {
            if (value < min)
                value = min;
            if (value > max)
                value = max;
            return value;
        }

        /**
         * \brief Check that given number is within specified inclusive bounds.
         * @tparam number Numeric type
         * @param value Number to check.
         * @param min Lower inclusive bound.
         * @param max Upper inclusive bound.
         * @return Whether the number falls into these bounds.
         */
        template<typename number>
        inline bool is_bounded(number value, number min, number max) {
            return min <= value && value <= max;
        }

        /**
         * \brief Convert float to integer.
         * Rounds the float to a whole number and then casts it to int.
         *
         * @param arg   float to convert
         * @return      converted integer
         */
        inline int roundToInt(const float arg) {
            return (int) std::round(arg);
        }

        /**
         * \brief Float PI
         */
        constexpr float PI_F = static_cast<float>(M_PI);
        /**
         * \brief Double PI
         */
        constexpr double PI = static_cast<double>(M_PI);
    }
}

#endif //ROBOFAT2_UTILITIES_NUMBERS_HPP
