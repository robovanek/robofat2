#ifndef ROBOFAT2_UTILITIES_STOPWATCH_HPP
#define ROBOFAT2_UTILITIES_STOPWATCH_HPP

#include <chrono>

namespace robofat2 {
    namespace utilities {
        class stopwatch {
            using clock = std::chrono::steady_clock;
            using duration = clock::duration;
            using time_point = clock::time_point;

        public:
            stopwatch();

            duration get_elapsed_since_mark() const;

            template<typename Type, typename Ratio>
            bool has_elapsed_since_mark(std::chrono::duration<Type, Ratio> duration) const {
                return get_elapsed_since_mark() >= duration;
            }

            void set_mark_to_now();

            void reset_mark();

            bool is_mark_empty() const;

            time_point get_mark() const;

            void set_mark(time_point point);

        private:
            time_point m_mark;
            bool m_empty;
        };
    }
}

#endif //ROBOFAT2_UTILITIES_STOPWATCH_HPP
