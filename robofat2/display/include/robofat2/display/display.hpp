/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Screen I/O.
 */

#ifndef ROBOFAT2_DISPLAY_HPP
#define ROBOFAT2_DISPLAY_HPP

#include <string>
#include <vector>

namespace robofat2 {
    namespace display {
        /**
         * \brief Start an interactive menu and return what the user has selected.
         *
         * @param prompt    Selection prompt.
         * @param options   Available options.
         * @param select    Default option index.
         * @return          Index of selected option or -1 if user cancelled the menu.
         */
        extern int show_menu(const std::string &prompt,
                             const std::vector<std::string> &options,
                             int select = 0);
    }
}

#endif //ROBOFAT2_DISPLAY_HPP
