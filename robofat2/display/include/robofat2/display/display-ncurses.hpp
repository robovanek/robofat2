#ifndef ROBOFAT2_DISPLAY_NCURSES_HPP
#define ROBOFAT2_DISPLAY_NCURSES_HPP


#include <ncurses.h>
#include <memory>

namespace robofat2 {
    namespace display {
        /**
         * NCurses main context.
         */
        class ncurses_ctx {
        public:
            /**
             * Initialize new NCurses context.
             */
            ncurses_ctx();

            /**
             * Destroy the current NCurses context.
             */
            ~ncurses_ctx();

            /**
             * Get the current terminal width.
             * @return Maximum number of characters that fit into the terminal window horizontally.
             */
            int get_width() const;

            /**
             * Get the current terminal height.
             * @return Maximum number of characters that fit into the terminal window vertically.
             */
            int get_height() const;
        };

        /**
         * NCurses window.
         */
        class window {
        public:
            /**
             * Create a new NCurses window/frame.
             *
             * @param x Requested X-coordinate of the window.
             * @param y Requested Y-coordinate of the window.
             * @param w Requested X-length of the window.
             * @param h Requested Y-length of the window.
             */
            window(int x, int y, int w, int h);

            /**
             * Destroy this window.
             */
            virtual ~window() = default;

            /**
             * Get the underlying NCurses handle.
             * @return Pointer to the WINDOW structure.
             */
            WINDOW *get() {
                return m_handle.get();
            }

        private:
            /**
             * Deleter for the WINDOW handle.
             * @param window Object to delete.
             */
            static void deleter(WINDOW *window);

            /**
             * Holder of the NCurses window handle.
             */
            std::unique_ptr<WINDOW, void (*)(WINDOW *)> m_handle;
        };

        /**
         * NCurses vertical menu window.
         */
        class menu_window : public window {
        public:
            /**
             * Create a new NCurses menu window.
             * @param ctx NCurses main context.
             * @param prompt Prompt for the user to select an option.
             * @param options List of the options to show.
             * @param dfl Index of the default selection.
             */
            menu_window(ncurses_ctx &ctx,
                        const std::string &prompt,
                        const std::vector<std::string> &options,
                        int dfl);

        private:
            /**
             * Create a new NCurses menu window.
             * @param ctx NCurses main context.
             * @param prompt Prompt for the user to select an option.
             * @param options List of the options to show.
             * @param dfl Index of the default selection.
             * @param opts Number of provided options.
             * @param maxLength Maximum length of an option string.
             */
            menu_window(ncurses_ctx &ctx,
                        std::string prompt,
                        std::vector<std::string> options,
                        int dfl,
                        int opts,
                        int maxLength);

        public:
            /**
             * Show the menu and wait for the result.
             * @return Index of the selected option or -1 if the menu was cancelled.
             */
            int show();

        private:
            /**
             * Wait until a key is pressed. Update the selected option index.
             * @return Whether this is just a up/down move (true) or a selection/cancellation (false).
             */
            bool keyLoop();

            /**
             * Display/redraw the entire menu.
             */
            void display();

            /**
             * Draw a single line of the menu.
             *
             * @param y Y-coordinate of the item.
             * @param x X-coordinate of the item.
             * @param inverted Whether the item should be displayed in inverted colors.
             * @param value Line contents.
             */
            void displayLine(int y, int x, bool inverted, const std::string &value);

            /**
             * Get maximum length of a string in the provided list.
             * @param prompt User prompt (will be effectively added to the list).
             * @param opts List of strings to walk through.
             * @return Length of the longest string, in bytes (without null terminator).
             */
            static int get_max_width(const std::string &prompt, const std::vector<std::string> &opts);

            /**
             * User prompt to select an option.
             */
            std::string m_prompt;
            /**
             * List of available options.
             */
            std::vector<std::string> m_options;
            /**
             * Index of the currently selected option.
             */
            int m_index = 0;
        };
    }
}

#endif //ROBOFAT2_DISPLAY_NCURSES_HPP
