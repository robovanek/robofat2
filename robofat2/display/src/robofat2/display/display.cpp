/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Screen I/O implementation.
 */

#include <robofat2/display/display.hpp>
#include <robofat2/display/display-ncurses.hpp>

using namespace robofat2::display;

int robofat2::display::show_menu(const std::string &prompt,
                                 const std::vector<std::string> &options,
                                 int select) {
    ncurses_ctx ctx;
    robofat2::display::menu_window menuWin(ctx, prompt, options, select);
    return menuWin.show();
}

ncurses_ctx::ncurses_ctx() {
    initscr(); // ncurses init
    cbreak(); // do not buffer
    noecho(); // do not echo input
    curs_set(0); // hide cursor
    scrollok(stdscr, true); // enable scrolling
    keypad(stdscr, true); // enable arrows

    clear(); // clear screen
    move(0, 0); // go to screen beginning
}

ncurses_ctx::~ncurses_ctx() {
    endwin();
}

int ncurses_ctx::get_width() const {
    return getmaxx(stdscr);
}

int ncurses_ctx::get_height() const {
    return getmaxy(stdscr);
}

window::window(int x, int y, int w, int h)
        : m_handle(newwin(h, w, std::max(y, 0), std::max(x, 0)), &deleter) {
    keypad(m_handle.get(), true); // allow arrows
    box(m_handle.get(), 0, 0); // draw border
    touchwin(m_handle.get()); // ??? probably to mark for redrawing
    wrefresh(m_handle.get());
}

void window::deleter(WINDOW *window) {
    wclear(window);
    wrefresh(window);
    delwin(window);
}

menu_window::menu_window(ncurses_ctx &ctx,
                         const std::string &prompt,
                         const std::vector<std::string> &options,
                         int dfl)
        : menu_window(ctx,
                      prompt,
                      options,
                      dfl,
                      options.size(),
                      get_max_width(prompt, options)) {}

menu_window::menu_window(ncurses_ctx &ctx,
                         std::string prompt,
                         std::vector<std::string> options,
                         int dfl,
                         int opts,
                         int maxLength)
        : window(ctx.get_width() / 2 - maxLength / 2 - 2,
                 ctx.get_height() / 2 - opts / 2 - 2,
                 maxLength + 5,
                 opts + 3),
          m_prompt(std::move(prompt)),
          m_options(std::move(options)),
          m_index(std::min(opts, std::max(dfl, 0))) {}

int menu_window::show() {
    do {
        display();
    } while (keyLoop());
    return m_index;
}

bool menu_window::keyLoop() {
    for (;;) {
        int ch = wgetch(get());
        switch (ch) {
            default:
                continue;
            case KEY_DOWN:
                m_index++;
                if (m_index >= (int) m_options.size())
                    m_index = 0;
                return true;
            case KEY_UP:
                m_index--;
                if (m_index < 0)
                    m_index = (int) m_options.size() - 1;
                return true;
            case KEY_ENTER:
            case '\n':
                return false;
            case KEY_BACKSPACE:
                m_index = -1;
                return false;
        }
    }
}

void menu_window::display() {
    displayLine(1, 2, false, m_prompt);
    for (int i = 0; i < (int) m_options.size(); i++) {
        displayLine(i + 2, 3,
                    i == m_index,
                    m_options[i]);
    }
    wrefresh(get());
}

void menu_window::displayLine(int y, int x, bool inverted, const std::string &value) {
    if (inverted)
        wattron(get(), A_REVERSE);
    mvwprintw(get(), y, x, value.c_str());
    if (inverted)
        wattroff(get(), A_REVERSE);
}

int menu_window::get_max_width(const std::string &prompt, const std::vector<std::string> &opts) {
    int max = prompt.length();
    for (auto &&str : opts) {
        max = std::max(max, (int) str.size());
    }
    return max;
}
