# Findncurses.cmake
#
# Finds the ncurses library
#
# This will define the following variables
#
#    NCURSES_FOUND
#
# and the following imported targets
#
#     NCURSES::ncurses
#
# Author: CMake 3.10
# Author: Jakub Vaněk - linuxtardis@gmail.com

find_library(NCURSES_LIBRARY NAMES ncurses)

get_filename_component(_cursesLibDir "${NCURSES_LIBRARY}" PATH)
get_filename_component(_cursesParentDir "${_cursesLibDir}" PATH)

find_path(NCURSES_INCLUDE_PATH
        NAMES ncurses.h curses.h
        HINTS "${_cursesParentDir}/include" "${_cursesParentDir}/include/ncurses"
        )

mark_as_advanced(NCURSES_FOUND NCURSES_LIBRARY NCURSES_INCLUDE_PATH)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ncurses
        REQUIRED_VARS NCURSES_LIBRARY NCURSES_INCLUDE_PATH)

if (NCURSES_FOUND AND NOT TARGET NCURSES::ncurses)
    add_library(NCURSES::ncurses INTERFACE IMPORTED)
    set_target_properties(NCURSES::ncurses PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${NCURSES_INCLUDE_PATH}"
            INTERFACE_LINK_LIBRARIES "${NCURSES_LIBRARY}"
            )
endif ()
