## Directory structure

Subdirectories:
* `build` - default build directory for the brick
* `CMake` - directory for CMake helper modules
* `googletest` - Google Test submodule for some unit tests
* `backtrace` - helper stub for catching uncaught exceptions. However it does not work properly on ARM because of missing `-funwind-tables` in libc.
* `udev` - libudev wrapper for providing an interface to ev3dev sysfs devices
* `devices` - LEGO sensor/motor interface implemented on top of libudev wrapper
* `robofat` - library for commonly used classes (PID, basic settings, timing, ...)
* `threading` - library for simple mainloop-based task scheduling
* `maps` - generic map & search for cartesian block/tile maps
* `roboconfino` - remote control/settings/logging library
* `robogui` - remote control application (QT-based)
* `ducttape` - module of the main program for the Mountain Climber task
* `ducttape/maps` - map & search specialization
* `ducttape/mechanics` - classes for maintaining robot movement (physics related - calculations, regulations, fixups, ...)
* `ducttape/spatial` - classes for decision making - where the robot should go on the virtual map
* `ducttape/sensorics` - class for reading & analyzing inputs from sensors
* `ducttape/movements` - sequencing of individual movements (i.e. parts of climb, fall, turn, etc) (= middle man between decisions and mechanics)
* `ducttape/main` - program startup & glue everything together

Files:
* `ducttape.properties` - current config file
* `conf_initial.sh` - script to generate (incomplete and raw) list of configuration entries
* `flow.dot` - Obsolete schema of the control flow in the `ducttape/movements` module
* `flow.dot.png` - PNG version of `flow.dot`
* `CMakeLists.txt` - main CMakeLists
* `CMakeLists.txt.user` - project for QT Creator
