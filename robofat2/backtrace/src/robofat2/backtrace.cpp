#include "robofat2/backtrace.hpp"
#include "robofat2/backtrace-impl.hpp"

#include <iostream>
#include <sstream>
#include <cstring>
#include <memory>

#include <cxxabi.h>
#include <backtrace.h>
#include <csignal>

void robofat2::backtrace::initialize() noexcept {
    try {
        std::set_terminate(&robofat2::backtrace::exception_hook);
    } catch (...) {
        exception_hook();
    }
}

void robofat2::backtrace::exception_hook() {
    std::exception_ptr eptr = std::current_exception();
    if (eptr) {
        bool cpp = false;
        std::cerr << "ERROR: std::terminate() called after an uncaught exception:" << std::endl;
        std::cerr << "- type:    " << demangle(eptr.__cxa_exception_type()->name(), cpp) << std::endl;
        try {
            std::rethrow_exception(eptr);
        } catch (const std::exception &e) {
            std::cerr << "- message: " << e.what() << std::endl;
        } catch (...) {
            std::cerr << "- message: <unknown>" << std::endl;
        }
    } else {
        std::cerr << "ERROR: std::terminate() called." << std::endl;
    }

    std::cerr << "Backtrace:" << std::endl;

    stack_walk_state info;
    backtrace_state *state = backtrace_create_state(nullptr, 0, &backtrace_error, nullptr);
    backtrace_full(state, 0, &backtrace_full_handler, &backtrace_error, &info);
    std::cerr << std::flush;

    // allow debugger to attach
    std::raise(SIGINT);

    // bye
    exit(1);
}

void robofat2::backtrace::backtrace_error(void *data, const char *msg, int errnum) {
    stack_walk_state &info = *reinterpret_cast<stack_walk_state *>(data);

    std::cerr << "* frame #" << info.call_depth << ": backtrace error occured: " << msg;
    if (errnum != -1) {
        std::cerr << "[errno=" << errnum << " ~ '" << strerror(errnum) << "']";
    }
    std::cerr << std::endl << std::flush;
}

std::string robofat2::backtrace::demangle(const char *function, bool &cpp) {
    int status = 0;
    char *demangled = abi::__cxa_demangle(function, nullptr, nullptr, &status);

    std::ostringstream ostr;
    cpp = false;
    switch (status) {
        case DEMANGLE_SUCCESS:
            ostr << demangled;
            cpp = true;
            break;
        case DEMANGLE_ALLOC_ERROR:
            ostr << "[" << function << "/memory alloc demangle error]";
            break;
        case DEMANGLE_NOT_CXX_MANGLED:
            ostr << function << "(...)";
            break;
        case DEMANGLE_INVALID_ARGUMENT:
            ostr << "[" << function << "/invalid argument demangle error]";
            break;
        default:
            ostr << "[" << function << "/unknown demangle error]";
            break;
    }
    free(demangled);
    return ostr.str();
}

int robofat2::backtrace::backtrace_full_handler(void *data, uintptr_t pc,
                                                const char *filename, int lineno,
                                                const char *function) {
    stack_walk_state &info = *reinterpret_cast<stack_walk_state *>(data);

    std::cerr << "* frame #" << info.call_depth << ": " << std::endl;

    bool cpp = false;
    std::cerr << "  - name:    ";
    if (function != nullptr) {
        std::cerr << demangle(function, cpp);
    } else {
        std::cerr << "[name unknown]";
    }
    std::cerr << std::endl;
    std::cerr << "  - linkage: " << (cpp ? "C++" : "C") << std::endl;
    std::cerr << "  - address: " << reinterpret_cast<const void *>(pc) << std::endl;

    std::cerr << "  - source:  ";
    if (filename != nullptr) {
        std::cerr << filename;
    } else {
        std::cerr << "unknown";
    }

    if (lineno != 0) {
        std::cerr << ":" << lineno;
    }
    std::cerr << std::endl << std::flush;
    info.call_depth++;
    if (function != nullptr && std::strcmp(function, "main") == 0) {
        return 1;
    } else {
        return 0;
    }
}
