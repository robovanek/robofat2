#ifndef ROBOFAT2_BACKTRACE_IMPL_HPP
#define ROBOFAT2_BACKTRACE_IMPL_HPP

#include <string>
#include <cstdint>

namespace robofat2 {
    namespace backtrace {

        /**
         * Structure for holding the backtrace state.
         */
        struct stack_walk_state {
            stack_walk_state() : call_depth(1) {}

            int call_depth;
        };

        /**
         * Result of the abi::__cxa_demangle() call.
         */
        enum demangle_result {
            DEMANGLE_SUCCESS = 0,
            DEMANGLE_ALLOC_ERROR = -1,
            DEMANGLE_NOT_CXX_MANGLED = -2,
            DEMANGLE_INVALID_ARGUMENT = -3
        };

        /**
         * Convert a possibly C++-mangled name to a human-readable one.
         * @param function Raw function name.
         * @param cpp Output for whether the function really was a C++ function.
         * @return String containing the function name (or an error message).
         */
        std::string demangle(const char *function, bool &cpp);

        /**
         * Stack frame handler for libbacktrace.
         *
         * This will print information about the current stack frame.
         * @param data Shared stack_walk_state structure for persisting call stack depth.
         * @param pc Program Counter of the place where the program was.
         * @param filename Name of the file where the function is.
         * @param lineno Location in the file where the function is.
         * @param function Raw function name where the program was. It may be C++-mangled; this function will tr to demangle it.
         * @return 1 for end of unwinding, 0 for continuation of unwinding.
         */
        int backtrace_full_handler(void *data, uintptr_t pc, const char *filename, int lineno, const char *function);

        /**
         * Error handler for libbacktrace.
         *
         * @param data Shared stack_walk_state structure for persisting call stack depth.
         * @param msg Message describing the error.
         * @param errnum POSIX errno value associated with the error.
         */
        void backtrace_error(void *data, const char *msg, int errnum);

    }
}


#endif //ROBOFAT2_BACKTRACE_IMPL_HPP
