#ifndef ROBOFAT2_BACKTRACE_HPP
#define ROBOFAT2_BACKTRACE_HPP

namespace robofat2 {
    namespace backtrace {
        /**
         * Exception entry point.
         *
         * Prints backtrace and either notifies a debugger (if present) or exits.
         */
        [[noreturn]] [[gnu::noinline]]
        extern void exception_hook();

        /**
         * Initialize the automatic backtrace support.
         *
         * This will register robofat::backtrace::exception_hook() as a std::terminate() handler.
         */
        extern void initialize() noexcept;
    }
}

#endif //ROBOFAT2_BACKTRACE_HPP
