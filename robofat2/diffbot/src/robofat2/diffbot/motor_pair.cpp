#include <robofat2/diffbot/motor_pair.hpp>
#include <robofat2/utilities/numbers.hpp>

using namespace robofat2::diffbot;
using namespace robofat2::utilities;
using namespace std::placeholders;
using namespace ev3dev;

motor_pair::motor_pair(const address_type &left,
                       const address_type &right) :
        m_leftMotor(left),
        m_rightMotor(right) {}

motor_pair::~motor_pair() {
    m_leftMotor.reset();
    m_rightMotor.reset();
}

void motor_pair::initialize_motors(int rampUp, int rampDown,
                                   int holdP, int holdI, int holdD,
                                   int speedP, int speedI, int speedD) {
    m_leftMotor.reset();
    m_rightMotor.reset();

    m_leftMotor.set_polarity(motor::polarity_normal);
    m_rightMotor.set_polarity(motor::polarity_normal);

    m_leftMotor.set_stop_action(motor::stop_action_hold);
    m_rightMotor.set_stop_action(motor::stop_action_hold);

    setup_field(rampUp, &motor::set_ramp_up_sp);
    setup_field(rampDown, &motor::set_ramp_down_sp);

    setup_field(holdP, &motor::set_position_p);
    setup_field(holdI, &motor::set_position_i);
    setup_field(holdD, &motor::set_position_d);
    setup_field(speedP, &motor::set_speed_p);
    setup_field(speedI, &motor::set_speed_i);
    setup_field(speedD, &motor::set_speed_d);
}

void motor_pair::setup_field(int value,
                             motor_setter setter) {
    (m_leftMotor.*setter)(value);
    (m_rightMotor.*setter)(value);
}

angular_pos_pair<int> motor_pair::get_target_position() {
    return {
            m_leftMotor.position_sp(),
            m_rightMotor.position_sp()
    };
}

angular_speed_pair<int> motor_pair::get_target_speed() {
    return {
            m_leftMotor.speed_sp(),
            m_rightMotor.speed_sp()
    };
}

angular_pos_pair<int> motor_pair::get_current_position() {
    return {
            m_leftMotor.position(),
            m_rightMotor.position()
    };
}

angular_speed_pair<int> motor_pair::get_current_speed() {
    return {
            m_leftMotor.speed(),
            m_rightMotor.speed()
    };
}

void motor_pair::start_unlimited(angular_speed_pair<int> speeds) {
    m_leftMotor.set_speed_sp(speeds.left);
    m_rightMotor.set_speed_sp(speeds.left);

    m_leftMotor.run_forever();
    m_rightMotor.run_forever();
}

void motor_pair::start_limited(angular_speed_pair<int> speeds, angular_pos_pair<int> positions) {
    m_leftMotor.set_speed_sp(speeds.left);
    m_rightMotor.set_speed_sp(speeds.left);

    m_leftMotor.set_position_sp(positions.left);
    m_rightMotor.set_position_sp(positions.left);

    m_leftMotor.run_to_abs_pos();
    m_rightMotor.run_to_abs_pos();
}

void motor_pair::stop() {
    m_leftMotor.stop();
    m_rightMotor.stop();
}

bool motor_pair::moving() {
    auto leftState = m_leftMotor.state();
    auto rightState = m_rightMotor.state();

    return (leftState.count(motor::state_running) && !leftState.count(motor::state_stalled)) ||
           (rightState.count(motor::state_running) && !rightState.count(motor::state_stalled));
}

void motor_pair::start_direct() {
    m_leftMotor.run_direct();
    m_rightMotor.run_direct();
}

void motor_pair::push_direct(power_pair<int> powers) {
    // allow only well-defined power levels
    powers.left = limit(powers.left, -100, +100);
    powers.right = limit(powers.right, -100, +100);
    // push to kernel
    m_leftMotor.set_duty_cycle_sp(powers.left);
    m_rightMotor.set_duty_cycle_sp(powers.right);
}
