#include <robofat2/diffbot/motor_types.hpp>

template
struct robofat2::diffbot::wheel_pair_base<float, robofat2::diffbot::linear_pos_pair<float>>;
template
struct robofat2::diffbot::wheel_pair_base<float, robofat2::diffbot::linear_speed_pair<float>>;
template
struct robofat2::diffbot::wheel_pair_base<float, robofat2::diffbot::angular_pos_pair<float>>;
template
struct robofat2::diffbot::wheel_pair_base<float, robofat2::diffbot::angular_speed_pair<float>>;
template
struct robofat2::diffbot::wheel_pair_base<float, robofat2::diffbot::power_pair<float>>;
template
struct robofat2::diffbot::wheel_pair_base<int, robofat2::diffbot::linear_pos_pair<int>>;
template
struct robofat2::diffbot::wheel_pair_base<int, robofat2::diffbot::linear_speed_pair<int>>;
template
struct robofat2::diffbot::wheel_pair_base<int, robofat2::diffbot::angular_pos_pair<int>>;
template
struct robofat2::diffbot::wheel_pair_base<int, robofat2::diffbot::angular_speed_pair<int>>;
template
struct robofat2::diffbot::wheel_pair_base<int, robofat2::diffbot::power_pair<int>>;

template
struct robofat2::diffbot::linear_pos_pair<float>;
template
struct robofat2::diffbot::linear_speed_pair<float>;
template
struct robofat2::diffbot::angular_pos_pair<float>;
template
struct robofat2::diffbot::angular_speed_pair<float>;
template
struct robofat2::diffbot::power_pair<float>;
template
struct robofat2::diffbot::linear_pos_pair<int>;
template
struct robofat2::diffbot::linear_speed_pair<int>;
template
struct robofat2::diffbot::angular_pos_pair<int>;
template
struct robofat2::diffbot::angular_speed_pair<int>;
template
struct robofat2::diffbot::power_pair<int>;
