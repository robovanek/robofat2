#include <robofat2/utilities/numbers.hpp>
#include <robofat2/diffbot/movement_model.hpp>

using namespace robofat2::diffbot;
using namespace robofat2::utilities;

movement_model::movement_model(float trackWidth, float wheelRadius)
        : m_trackWidth(trackWidth),
          m_wheelRadius(wheelRadius) {}

float movement_model::linear_to_angular(float linearDistance) const {
    return 360.0f * linearDistance / (2.0f * PI_F * m_wheelRadius);
}

float movement_model::angular_to_linear(float wheelAngle) const {
    return wheelAngle / 360.0f * (2 * PI_F * m_wheelRadius);
}

float movement_model::calculate_travelled_distance(angular_pos_pair<int> tachoDeltas) const {
    float averageDelta = (float) (tachoDeltas.left + tachoDeltas.right) / 2.0f;
    return angular_to_linear(averageDelta);
}

float movement_model::calculate_rotated_angle(angular_pos_pair<int> tachoDeltas) const {
    return (float) (tachoDeltas.right - tachoDeltas.left) * m_wheelRadius / m_trackWidth;
}

angular_pos_pair<int> movement_model::calculate_tacho_for_rotation(int angle) const {
    float wheelAngle = (float) angle * m_trackWidth / (2.0f * m_wheelRadius);
    int wheelAngleInt = roundToInt(wheelAngle);
    return {-wheelAngleInt, +wheelAngleInt};
}

angular_pos_pair<int> movement_model::calculate_tacho_for_travel(float linearMM) const {
    float wheelAngle = linear_to_angular(linearMM);
    int wheelAngleInt = roundToInt(wheelAngle);
    return {+wheelAngleInt, +wheelAngleInt};
}

angular_speed_pair<int> movement_model::get_angular_speed_pair(float linearMM) const {
    float angular = linear_to_angular(linearMM);
    int angularInt = roundToInt(angular);
    return {angularInt, angularInt};
}
