#ifndef ROBOFAT2_DIFFBOT_MOVEMENT_MODEL_HPP
#define ROBOFAT2_DIFFBOT_MOVEMENT_MODEL_HPP

#include <robofat2/diffbot/motor_types.hpp>

namespace robofat2 {
    namespace diffbot {
        class movement_model {
        public:
            explicit movement_model(float trackWidth, float wheelRadius);

            // linear distance to wheel angle
            float linear_to_angular(float linearDistance) const;

            // wheel angle to linear distance
            float angular_to_linear(float wheelAngle) const;

            float calculate_travelled_distance(angular_pos_pair<int> tachoDeltas) const;

            float calculate_rotated_angle(angular_pos_pair<int> tachoDeltas) const;

            angular_pos_pair<int> calculate_tacho_for_rotation(int angle) const;

            angular_pos_pair<int> calculate_tacho_for_travel(float linearMM) const;

            angular_speed_pair<int> get_angular_speed_pair(float linearMM) const;

        private:
            float m_trackWidth;
            float m_wheelRadius;
        };
    }
}

#endif //ROBOFAT2_DIFFBOT_MOVEMENT_MODEL_HPP
