#ifndef ROBOFAT2_DIFFBOT_MOTOR_TYPES_HPP
#define ROBOFAT2_DIFFBOT_MOTOR_TYPES_HPP

#include <utility>
#include <type_traits>
#include <sstream>

namespace robofat2 {
    namespace diffbot {
        template<typename T, typename DerivedT>
        struct wheel_pair_base {
            using element_type = T;
            using container_type = DerivedT;

            wheel_pair_base() = default;

            explicit
            wheel_pair_base(T both)
                    : left(both), right(both) {}

            wheel_pair_base(T left, T right)
                    : left(left), right(right) {}

            bool operator==(const DerivedT &other) const {
                return this->left == other.left &&
                       this->right == other.right;
            }

            bool operator!=(const DerivedT &other) const {
                return !(*this == other);
            }

            DerivedT operator+(const DerivedT &other) const {
                return {
                        this->left + other.left,
                        this->right + other.right
                };
            }

            DerivedT operator-(const DerivedT &other) const {
                return {
                        this->left - other.left,
                        this->right - other.right
                };
            }

            DerivedT operator*(T factor) const {
                return {
                        this->left * factor,
                        this->right * factor
                };
            }

            DerivedT operator/(T factor) const {
                return {
                        this->left / factor,
                        this->right / factor
                };
            }

            std::string as_string() const {
                std::ostringstream ss;
                ss << "(L:" << left << ", R:" << right << ")";
                return ss.str();
            }

            T average() const {
                return (this->left + this->right) / T{2};
            }

            T left;
            T right;
        };

        template<typename T>
        struct linear_pos_pair : public wheel_pair_base<T, linear_pos_pair<T>> {
            linear_pos_pair() = default;

            explicit
            linear_pos_pair(T both) : wheel_pair_base<T, linear_pos_pair<T>>(both) {}

            linear_pos_pair(T left, T right) : wheel_pair_base<T, linear_pos_pair<T>>(left, right) {}
        };

        template<typename T>
        struct linear_speed_pair : public wheel_pair_base<T, linear_speed_pair<T>> {
            linear_speed_pair() = default;

            explicit
            linear_speed_pair(T both) : wheel_pair_base<T, linear_speed_pair<T>>(both) {}

            linear_speed_pair(T left, T right) : wheel_pair_base<T, linear_speed_pair<T>>(left, right) {}
        };

        template<typename T>
        struct angular_pos_pair : public wheel_pair_base<T, angular_pos_pair<T>> {
            angular_pos_pair() = default;

            explicit
            angular_pos_pair(T both) : wheel_pair_base<T, angular_pos_pair<T>>(both) {}

            angular_pos_pair(T left, T right) : wheel_pair_base<T, angular_pos_pair<T>>(left, right) {}
        };

        template<typename T>
        struct angular_speed_pair : public wheel_pair_base<T, angular_speed_pair<T>> {
            angular_speed_pair() = default;

            explicit
            angular_speed_pair(T both) : wheel_pair_base<T, angular_speed_pair<T>>(both) {}

            angular_speed_pair(T left, T right) : wheel_pair_base<T, angular_speed_pair<T>>(left, right) {}
        };

        template<typename T>
        struct power_pair : public wheel_pair_base<T, power_pair<T>> {
            power_pair() = default;

            explicit
            power_pair(T both) : wheel_pair_base<T, power_pair<T>>(both) {}

            power_pair(T left, T right) : wheel_pair_base<T, power_pair<T>>(left, right) {}
        };

        template<typename DstT, typename SrcT>
        DstT pair_cast(SrcT src) {
            return DstT{
                    static_cast<typename DstT::element_type>(src.left),
                    static_cast<typename DstT::element_type>(src.right)
            };
        }

        extern template
        struct wheel_pair_base<float, linear_pos_pair<float>>;
        extern template
        struct wheel_pair_base<float, linear_speed_pair<float>>;
        extern template
        struct wheel_pair_base<float, angular_pos_pair<float>>;
        extern template
        struct wheel_pair_base<float, angular_speed_pair<float>>;
        extern template
        struct wheel_pair_base<float, power_pair<float>>;
        extern template
        struct wheel_pair_base<int, linear_pos_pair<int>>;
        extern template
        struct wheel_pair_base<int, linear_speed_pair<int>>;
        extern template
        struct wheel_pair_base<int, angular_pos_pair<int>>;
        extern template
        struct wheel_pair_base<int, angular_speed_pair<int>>;
        extern template
        struct wheel_pair_base<int, power_pair<int>>;

        extern template
        struct linear_pos_pair<float>;
        extern template
        struct linear_speed_pair<float>;
        extern template
        struct angular_pos_pair<float>;
        extern template
        struct angular_speed_pair<float>;
        extern template
        struct power_pair<float>;
        extern template
        struct linear_pos_pair<int>;
        extern template
        struct linear_speed_pair<int>;
        extern template
        struct angular_pos_pair<int>;
        extern template
        struct angular_speed_pair<int>;
        extern template
        struct power_pair<int>;
    }
}

#endif //ROBOFAT2_DIFFBOT_MOTOR_TYPES_HPP
