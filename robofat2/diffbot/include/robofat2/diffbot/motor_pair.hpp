#ifndef ROBOFAT2_DIFFBOT_MOTOR_PAIR_HPP
#define ROBOFAT2_DIFFBOT_MOTOR_PAIR_HPP

#include <ev3dev.h>
#include <robofat2/diffbot/motor_types.hpp>

namespace robofat2 {
    namespace diffbot {
        class motor_pair {
        public:
            explicit motor_pair(const ev3dev::address_type &left,
                                const ev3dev::address_type &right);

            ~motor_pair();

            void initialize_motors(int rampUp, int rampDown,
                                   int holdP, int holdI, int holdD,
                                   int speedP, int speedI, int speedD);

            angular_pos_pair<int> get_target_position();

            angular_speed_pair<int> get_target_speed();

            angular_pos_pair<int> get_current_position();

            angular_speed_pair<int> get_current_speed();

            void start_unlimited(angular_speed_pair<int> speeds);

            void start_limited(angular_speed_pair<int> speeds, angular_pos_pair<int> positions);

            /**
             * Start the run-direct mode, in which the motor can be controlled directly
             * (kernel-based regulation is bypassed).
             */
            void start_direct();

            /**
             * Push raw PWM duty-cycle to the kernel.
             */
            void push_direct(power_pair<int> powers);

            void stop();

            bool moving();

        private:
            using motor_setter = ev3dev::motor &(ev3dev::motor::*)(int);
            void setup_field(int value,
                             motor_setter setter);

            ev3dev::motor m_leftMotor;
            ev3dev::motor m_rightMotor;
        };
    }
}

#endif //ROBOFAT2_DIFFBOT_MOTOR_PAIR_HPP
