#ifndef ROBOFAT2_MAPS_GENERIC_MAP_HPP
#define ROBOFAT2_MAPS_GENERIC_MAP_HPP

#include <cstddef>
#include <array>
#include <vector>
#include <tuple>
#include <robofat2/maps/position.hpp>

namespace robofat2 {
    namespace maps {
        /**
         * Generic 2D cartesian tile map.
         * @tparam T Tile type / stored info for each map position.
         */
        template<typename T>
        class tile_map {
        public:
            /**
             * Initialize a new instance of a map.
             * @param width Map width in tiles.
             * @param height Map height in tiles.
             * @param initial Initial contents in the defined area of the map.
             * @param default_ Contents in the undefined area of the map.
             */
            tile_map(int width, int height, T initial, T default_)
                    : m_width(width),
                      m_height(height),
                      m_array(m_width * m_height),
                      m_default(default_) {
                erase(initial);
            }

            /**
             * Erase the map by filling all defined positions/locations with the given tile.
             * @param initial Tile to overwrite the map with.
             */
            void erase(T initial) {
                m_array.assign(m_width * m_height, initial);
            }

            /**
             * Get the tile stored at the given position.
             * @param where Query position.
             * @return Tile at that position.
             */
            T get(const pos &where) const {
                if (isIndexValid(where)) {
                    return deref(where);
                }
                return m_default;
            }

            /**
             * Set the tile stored at the give position to a given value.
             * @param where Store position.
             * @param t Value to be stored.
             */
            void set(const pos &where, T t) {
                if (isIndexValid(where)) {
                    deref(where) = t;
                }
            }

            /**
             * Check whether the given position is in the defined area of the map.
             * @param where Position to verify.
             * @return True if the position is in the defined area, false otherwise.
             */
            bool isIndexValid(const pos &where) const {
                return 0 <= where.x() && where.x() < m_width &&
                       0 <= where.y() && where.y() < m_height;
            }

            /**
             * Get the map width.
             * @return Map width in tiles.
             */
            size_t width() const {
                return m_width;
            }

            /**
             * Get the map height.
             * @return Map height in tiles.
             */
            size_t height() const {
                return m_height;
            }

        private:
            /**
             * Get index to a flat array where the tile is stored.
             * @param where Position of a tile.
             * @return Array index for accessing the tile.
             */
            int getIndexOffset(const pos &where) const {
                return where.y() * m_width + where.x();
            }

            /**
             * Get a value at a given position.
             * @param where Position to look at.
             * @return Tile (value).
             */
            T deref(const pos &where) const {
                return m_array[getIndexOffset(where)];
            }

            /**
             * Get a reference to a given position.
             * @param where Position to look at.
             * @return Tile (reference).
             */
            T &deref(const pos &where) {
                return m_array[getIndexOffset(where)];
            }

            /**
             * Map width in tiles.
             */
            int m_width;
            /**
             * Map height in tiles.
             */
            int m_height;
            /**
             * Flat array that is used as a backing storage.
             */
            std::vector<T> m_array;
            /**
             * Tile value that is used for undefined areas of map.
             */
            T m_default;
        };
    }
}

#endif //ROBOFAT2_MAPS_GENERIC_MAP_HPP
