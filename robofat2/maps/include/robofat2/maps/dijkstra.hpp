#ifndef ROBOFAT2_MAPS_GENERIC_DIJKSTRA_HPP
#define ROBOFAT2_MAPS_GENERIC_DIJKSTRA_HPP

#include <cstdint>
#include <vector>
#include <queue>
#include <unordered_map>
#include <forward_list>
#include <limits>
#include <functional>

#include <robofat2/maps/position.hpp>
#include <robofat2/maps/dijkstra_node.hpp>

namespace robofat2 {
    namespace maps {
        /**
         * Type definitions for Dijkstra.
         */
        struct dijkstra_meta {
            /**
             * Numeric type for distance between positions/nodes = graph edge lengths.
             */
            using distance_t = unsigned int;
            /**
             * Functor type for querying distance between two positions.
             */
            using sensor_t = std::function<distance_t(const node &origin, const node &next)>;
            /**
             * Functor type for querying whether a position should end the search.
             */
            using finished_t = std::function<bool(const pos &now)>;

            /**
             * Priority queue type for sorting nodes by distance from origin.
             */
            using pq_t = std::priority_queue<node *, std::vector<node *>, distance_comparer>;
            /**
             * Mapping type for mapping positions to nodes.
             */
            using mapping_t = std::unordered_map<pos, node *>;
            /**
             * Storage type for storing nodes.
             */
            using storage_t = std::forward_list<node>;

            /**
             * Special edge length / distance between nodes representing no connection at all.
             */
            static constexpr distance_t nonexistent_edge = std::numeric_limits<distance_t>::max();
        };

        /**
         * Implementation of a Dijkstra shortest path search algorithm.
         *
         * This algorithm is similar to the Breadth-First-Search algorithm.
         * In that algorithm, nodes are taken from the start and then searched
         * in layers radiating from the centre / origin.
         *
         * Breadth-first-search would be a perfectly fine shortest-path-finding
         * algorithm, but there is a problem - it effectively requires that distance
         * between all neighboring nodes (= weight = edge length) is equal to one.
         *
         * Dijkstra is, on the other hand, more general. It accepts edge weights/lengths
         * that are non-negative integers. To achieve this, next nodes to explore
         * are not determined by the hop-count layering of nodes. Instead, the
         * current shortest distance to each node is tracked by the algorithm. Then, the
         * next node to explore is the currently nearest non-finished node.
         * The layering is still there - it is just not driven by adjacencies of
         * the nodes, but through the distance from the origin.
         */
        class dijkstra : public dijkstra_meta {
        public:
            /**
             * Initialize a Dijkstra algo run.
             * @param start Starting position.
             * @param map Functor for determining distance between two positions.
             * @param finished Functor for determining whether a position is a finish / ends the search.
             */
            explicit dijkstra(pos start, sensor_t map, finished_t finished);

            /**
             * Perform the graph path search.
             * @return Found path - an ordered sequence of positions from origin to finish.
             *         If an empty queue is returned, the path does not exist.
             *         If an one-element queue is returned, we're already at finish.
             */
            std::deque<pos> operator()();

        private:
            /**
             * Get a node associated with a given position.
             *
             * If it doesn't exist yet, allocate a new one and return that.
             * @param where Position of the node.
             * @return Graph node for the given position.
             */
            node *get_node(pos where);

            /**
             * Schedule the next node for further search if the path from this parent is shorter that the previous one.
             * @param next Node to schedule.
             * @param parent Its possible new parent.
             * @param edge Distance between parent and next.
             */
            void schedule_node_if_shorter(node *next, node *parent, distance_t edge);

            /**
             * Get the next scheduled node which currently has shortest path from the origin.
             * @return Nearest scheduled node.
             */
            node *pop_nearest_open_node();

            /**
             * Backtrack a found path from finish back to start.
             * @param end Finished node.
             * @return Path from start to finish.
             */
            static std::deque<pos> backtrack(node *end);

            /**
             * Distance determining functor.
             */
            sensor_t m_map;
            /**
             * Finish determining functor.
             */
            finished_t m_finished;
            /**
             * Minimum-first-queue for getting nearest scheduled nodes.
             */
            pq_t m_distance_sorter;
            /**
             * Mapping from map positions to graph node pointers.
             */
            mapping_t m_pos_to_node_map;
            /**
             * Storage for graph nodes.
             */
            storage_t m_allocation_holder;
        };
    }
}

#endif //ROBOFAT2_MAPS_GENERIC_DIJKSTRA_HPP
