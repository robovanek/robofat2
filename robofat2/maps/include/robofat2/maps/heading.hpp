#ifndef ROBOFAT2_MAPS_HEADING_HPP
#define ROBOFAT2_MAPS_HEADING_HPP

#include <robofat2/maps/tile_map.hpp>

namespace robofat2 {
    namespace maps {
        /**
         * Helper class for calculating azimuth differences/deltas.
         *
         * This class follows the physics sign convention:
         *   - CW rotations are negative
         *   - CCW rotations are positive.
         */
        class heading {
        public:
            /**
             * Initialize a new empty heading.
             */
            heading();

            /**
             * Initialize a new heading from a map orientation.
             * @param direction
             */
            explicit heading(orient direction);

            /**
             * Initialize a new heading from a gyro-reported angle.
             * @param angle
             */
            explicit heading(int angle);

            /**
             * Check whether two headings are equal.
             * @param other Heading to compare this heading to.
             * @return True when they are equal, false otherwise.
             */
            bool operator==(const heading &other) const;

            /**
             * Check whether two headings are different.
             * @param other Heading to compare this heading to.
             * @return True when they are different, false otherwise.
             */
            bool operator!=(const heading &other) const;

            /**
             * Get shortest rotation to a different heading.
             * @param other Heading to rotate to.
             * @return Angle delta in range [-180°; 180°).
             */
            int shortestPathTo(const heading &other) const;

            /**
             * Get the map orientation that is heading is nearest to.
             * @return Nearest map orientation.
             */
            orient nearest_direction() const;

            /**
             * Normalized azimuth that represents this heading.
             */
            int angle;

        private:
            /**
             * Get the clockwise path to the other heading.
             * @param other The heading to get to.
             * @return Negative angle delta.
             */
            int cwPathTo(const heading &other) const;

            /**
             * Get the counter-clockwise path to the other heading.
             * @param other The heading to get to.
             * @return Positive angle delta.
             */
            int ccwPathTo(const heading &other) const;

            /**
             * Normalize the given gyro azimuth to a heading azimuth.
             * @param angle Input azimuth.
             * @return Azimuth normalized to range [0°, 360°)
             */
            static int normalize(int angle);

            /**
             * Convert the given map orientation to a normalized azimuth.
             * @param direction Map orientation to convert.
             * @return Normalized azimuth pointing in the given direction.
             */
            static int orient_to_angle(orient direction);
        };
    }
}

#endif //ROBOFAT2_MAPS_HEADING_HPP
