#ifndef ROBOFAT2_MAPS_DIJKSTRA_NODE_HPP
#define ROBOFAT2_MAPS_DIJKSTRA_NODE_HPP

#include <cstdint>
#include <robofat2/maps/position.hpp>

namespace robofat2 {
    namespace maps {
        struct node;
        /**
         * Distance comparison functor for graph nodes.
         */
        struct distance_comparer {
            bool operator()(const node *a, const node *b) const;
        };

        /**
         * Graph node for Dijkstra path search.
         */
        struct node {
            /**
             * Node through which we got here, or nullptr if this is the origin.
             */
            node *parent = nullptr;
            /**
             * Shortest distance to this node's position from the origin.
             */
            unsigned int distance = UINT32_MAX;
            /**
             * Map position of this node.
             */
            const pos coord;
            /**
             * Whether this node has been already processed.
             */
            bool visited = false;

            /**
             * Initialize a new graph node
             * @param where Where this node lies on the map.
             */
            explicit node(pos where);

            /**
             * Return an adjacency list for this node.
             * @return List of positions adjacent to this node's position.
             */
            std::array<pos, 4> neighbors() const;

            /**
             * Check if two nodes point to the same position.
             * @param other Node to compare this node to.
             * @return True if these nodes point to the same position, false otherwise.
             */
            bool operator==(const node &other) const;

            /**
             * Check if two nodes point to a different positions.
             * @param other Node to compare this node to.
             * @return True if these nodes point to a different position, false otherwise.
             */
            bool operator!=(const node &other) const;
        };
    }
}

#endif //ROBOFAT2_MAPS_DIJKSTRA_NODE_HPP
