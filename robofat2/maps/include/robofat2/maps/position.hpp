#ifndef ROBOFAT2_MAPS_POSITION_HPP
#define ROBOFAT2_MAPS_POSITION_HPP

#include <string>
#include <tuple>
#include <robofat2/maps/orientation.hpp>

namespace robofat2 {
    namespace maps {
        /**
         * 2D map location class.
         */
        class pos : public std::tuple<int, int> {
        public:
            /**
             * Initialize default map location = [0, 0].
             */
            constexpr pos() noexcept : tuple(0, 0) {}

            /**
             * Initialize a new map location.
             * @param x X coordinate on the map.
             * @param y Y coordinate on the map.
             */
            constexpr pos(int x, int y) noexcept : tuple(x, y) {}

            /**
             * Get the X map coordinate.
             * @return Distance from the leftmost map tile.
             */
            int x() const noexcept {
                return std::get<0>(*this);
            }

            /**
             * Get the Y map coordinate.
             * @return Distance from the topmost map tile.
             */
            int y() const noexcept {
                return std::get<1>(*this);
            }

            /**
             * Move the current position along the specified orientation.
             * @param dir Map orientation to move along.
             * @param amount How many times to move forward.
             * @return Transformed position.
             */
            pos advance(orient dir, int amount = 1) const;

            /**
             * Generate an ordered list of neighbors.
             *
             * First will be the forward-facing tile.
             * Then the side tiles will follow.
             * The tile on the opposite side will be the last.
             *
             * @param previous Previous position (or nullptr)
             * @return
             */
            std::array<pos, 4> optimized_neighbors(const pos *previous) const;

            /**
             * Get the map orientation along which another position is located.
             * @param other Position to get to.
             * @throws std::logic_error if *this == other
             * @throws std::logic_error if the other position is not reachable by a single advance() call.
             * @return Orient along which it is possible to get to the other position.
             */
            orient direction_to(const pos &other) const;

            /**
             * Convert this location to a string representation.
             * @return pos[x; y]
             */
            std::string as_string() const;
        };
    }
}


namespace std {
    /**
     * Hash function for map location object.
     */
    template<>
    struct hash<robofat2::maps::pos> {
        size_t operator()(const robofat2::maps::pos &pos) const {
            uint value = ((uint) pos.y() << 16u) + (uint) pos.x();
            return static_cast<size_t>(value);
        }
    };
}

#endif //ROBOFAT2_MAPS_POSITION_HPP
