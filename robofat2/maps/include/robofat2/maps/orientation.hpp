#ifndef ROBOFAT2_MAPS_ORIENTATION_HPP
#define ROBOFAT2_MAPS_ORIENTATION_HPP

#include <string>
#include <array>

namespace robofat2 {
    namespace maps {
        /**
         * Direction/orientation on a map.
         */
        enum class orient {
            /**
             * North / upwards.
             */
                    north,
            /**
             * South / downwards.
             */
                    south,
            /**
             * East / rightwards.
             */
                    east,
            /**
             * West / leftwards.
             */
                    west
        };

        /**
         * Where to rotate a map direction.
         */
        enum class rotation {
            /**
             * To left / the front will go counter-clockwise.
             */
                    left,
            /**
             * To right / the front will go clockwise.
             */
                    right
        };

        /**
         * Relative position between map orientations.
         */
        enum class relative_position {
            /**
             * They're the same.
             */
                    new_same,
            /**
             * The new one is more to the left / counter-clockwise.
             */
                    new_to_left,
            /**
             * The new one is more to the right / clockwise.
             */
                    new_to_right,
            /**
             * The new one is opposite to the current one.
             */
                    new_opposite
        };

        /**
         * Get an array of all possible map orientations.
         */
        extern const std::array<orient, 4> all_orients;

        /**
         * Convert a map orient to a string representation.
         * @param direction Orientation to describe.
         * @return Name of the orient as a string.
         */
        std::string orient_as_string(orient direction);

        /**
         * Rotate the given map orientation in the given direction.
         * @param orig Starting orientation.
         * @param rot Where to rotate.
         * @return Transformed orientation.
         */
        orient rotate(orient orig, rotation rot);

        /**
         * Rotate the given map orientation in the given direction.
         * @param orig Starting orientation.
         * @param rot Where to rotate.
         * @param amount How many times to rotate.
         * @return Transformed orientation.
         */
        orient rotate(orient orig, rotation rot, int amount);

        /**
         * Compare the two map orientations.
         * @param oldO Starting orientation.
         * @param newO Finishing orientation.
         * @return Relative movement from the starting to the finishing orientation.
         */
        relative_position compare_directions(orient oldO, orient newO);
    }
}

#endif //ROBOFAT2_MAPS_ORIENTATION_HPP
