#include <robofat2/maps/position.hpp>
#include <sstream>

using namespace robofat2::maps;

pos pos::advance(orient dir, int amount) const {
    switch (dir) {
        case orient::north:
            return {x(), y() - amount};
        case orient::south:
            return {x(), y() + amount};
        case orient::east:
            return {x() + amount, y()};
        case orient::west:
            return {x() - amount, y()};
        default:
            return *this;
    }
}

orient pos::direction_to(const pos &other) const {
    if (*this == other) {
        throw std::logic_error("pos::direction_to() called with target=source");
    }
    if (other.x() == this->x()) {
        if (other.y() > this->y()) {
            return orient::south;
        } else {
            return orient::north;
        }
    } else if (other.y() == this->y()) {
        if (other.x() > this->x()) {
            return orient::east;
        } else {
            return orient::west;
        }
    } else {
        throw std::logic_error("pos::direction_to() called with a target that is not reachable in any direction");
    }
}

std::array<pos, 4>
pos::optimized_neighbors(const pos *previous) const {
    if (previous == nullptr) {
        std::array<pos, 4> result;
        for (size_t i = 0; i < all_orients.size(); i++) {
            result[i] = advance(all_orients[i]);
        }
        return result;
    } else {
        orient base = previous->direction_to(*this);
        return {
                advance(base),
                advance(rotate(base, rotation::right)),
                advance(rotate(base, rotation::left)),
                advance(rotate(base, rotation::left, 2))
        };
    }
}

std::string
pos::as_string() const {
    std::ostringstream ss;
    ss << "pos[" << x() << "; " << y() << "]";
    return ss.str();
}
