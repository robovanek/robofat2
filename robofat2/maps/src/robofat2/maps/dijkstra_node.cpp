#include <robofat2/maps/dijkstra_node.hpp>

using namespace robofat2::maps;

bool distance_comparer::operator()(const node *a, const node *b) const {
    return a->distance > b->distance;
}

node::node(pos where) : coord(std::move(where)) {}

bool node::operator==(const node &other) const {
    return this->coord == other.coord;
}

bool node::operator!=(const node &other) const {
    return !(this->coord == other.coord);
}

std::array<pos, 4> node::neighbors() const {
    return coord.optimized_neighbors(nullptr);
}
