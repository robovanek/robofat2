#include <robofat2/maps/dijkstra.hpp>

using namespace robofat2::maps;

std::deque<pos>
dijkstra::backtrack(node *end) {
    std::deque<pos> result;

    while (end != nullptr) {
        result.push_front(end->coord);
        end = end->parent;
    }
    return result;
}


dijkstra::dijkstra(pos startPos,
                   sensor_t map,
                   finished_t finished)
        : m_map(std::move(map)),
          m_finished(std::move(finished)),
          m_distance_sorter(),
          m_pos_to_node_map(),
          m_allocation_holder() {

    node *start = get_node(startPos);
    schedule_node_if_shorter(start, nullptr, 0);
}


std::deque<pos>
dijkstra::operator()() {
    while (!m_distance_sorter.empty()) {
        node *base = pop_nearest_open_node();

        if (!base->visited) {
            base->visited = true;
        } else {
            continue;
        }

        if (m_finished(base->coord)) {
            return backtrack(base);
        }

        // note: re-ordering of nodes here will have no effect
        // this is because the nodes will be reordered by
        // the priority queue anyways
        for (pos nextPos : base->neighbors()) {
            node *next = get_node(nextPos);

            distance_t edge = m_map(*base, *next);
            if (edge == nonexistent_edge) {
                continue;
            }

            schedule_node_if_shorter(next, base, edge);
        }
    }

    return {};
}


node *dijkstra::get_node(pos where) {
    auto &&iter = m_pos_to_node_map.find(where);
    if (iter != m_pos_to_node_map.end()) {
        return iter->second;
    } else {
        m_allocation_holder.emplace_front(where);
        node *ptr = &m_allocation_holder.front();
        m_pos_to_node_map.emplace(where, ptr);
        return ptr;
    }
}


node *dijkstra::pop_nearest_open_node() {
    node *node = m_distance_sorter.top();
    m_distance_sorter.pop();
    return node;
}


void dijkstra::schedule_node_if_shorter(node *next,
                                        node *parent,
                                        dijkstra_meta::distance_t edge) {
    distance_t parentDistance = parent == nullptr ? 0 : parent->distance;
    distance_t oldDistance = next->distance;
    distance_t newDistance = parentDistance + edge;
    if (newDistance < oldDistance) {
        next->distance = newDistance;
        next->parent = parent;
        m_distance_sorter.push(next);
    }
}
