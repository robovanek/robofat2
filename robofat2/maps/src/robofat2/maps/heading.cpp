#include <robofat2/maps/heading.hpp>
#include <robofat2/utilities/numbers.hpp>

using namespace robofat2::maps;
using namespace robofat2::utilities;

heading::heading()
        : heading(orient::north) {}

heading::heading(orient direction)
        : angle(orient_to_angle(direction)) {}

heading::heading(int angle)
        : angle(normalize(angle)) {}

bool heading::operator==(const heading &other) const {
    return this->angle == other.angle;
}

bool heading::operator!=(const heading &other) const {
    return !(*this == other);
}

orient heading::nearest_direction() const {
    if (is_bounded(angle, 90 - 45, 90 + 45)) {
        return orient::west;
    } else if (is_bounded(angle, 180 - 45, 180 + 45)) {
        return orient::south;
    } else if (is_bounded(angle, 270 - 45, 270 + 45)) {
        return orient::east;
    } else {
        return orient::north;
    }
}

int heading::normalize(int angle) {
    int mod1 = angle % 360;
    int plus = mod1 + 360;
    int mod2 = plus % 360;
    return mod2;
}

int heading::orient_to_angle(orient direction) {
    switch (direction) {
        default:
        case orient::north:
            return 0;
        case orient::west:
            return 90;
        case orient::south:
            return 180;
        case orient::east:
            return 270;
    }
}

int heading::shortestPathTo(const heading &other) const {
    int cw = cwPathTo(other);
    int ccw = ccwPathTo(other);
    if (std::abs(cw) <= std::abs(ccw)) {
        return cw;
    } else {
        return ccw;
    }
}

int heading::cwPathTo(const heading &other) const {
    int start = this->angle;
    int end = other.angle;

    if (start >= end) {
        return -(start - end);
    } else {
        return -(360 - (end - start));
    }
}

int heading::ccwPathTo(const heading &other) const {
    int start = this->angle;
    int end = other.angle;

    if (end >= start) {
        return end - start;
    } else {
        return 360 - (start - end);
    }
}
