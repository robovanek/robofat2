#include <robofat2/maps/orientation.hpp>

using namespace robofat2::maps;

const std::array<orient, 4> robofat2::maps::all_orients = {
        orient::north,
        orient::south,
        orient::east,
        orient::west
};

relative_position
robofat2::maps::compare_directions(orient oldO, orient newO) {
    switch (oldO) {
        case orient::north:
            switch (newO) {
                case orient::north:
                    return relative_position::new_same;
                case orient::south:
                    return relative_position::new_opposite;
                case orient::east:
                    return relative_position::new_to_right;
                case orient::west:
                    return relative_position::new_to_left;
                default:
                    return relative_position::new_same;
            }
        case orient::south:
            switch (newO) {
                case orient::north:
                    return relative_position::new_opposite;
                case orient::south:
                    return relative_position::new_same;
                case orient::east:
                    return relative_position::new_to_left;
                case orient::west:
                    return relative_position::new_to_right;
                default:
                    return relative_position::new_same;
            }
        case orient::east:
            switch (newO) {
                case orient::north:
                    return relative_position::new_to_left;
                case orient::south:
                    return relative_position::new_to_right;
                case orient::east:
                    return relative_position::new_same;
                case orient::west:
                    return relative_position::new_opposite;
                default:
                    return relative_position::new_same;
            }
        case orient::west:
            switch (newO) {
                case orient::north:
                    return relative_position::new_to_right;
                case orient::south:
                    return relative_position::new_to_left;
                case orient::east:
                    return relative_position::new_opposite;
                case orient::west:
                    return relative_position::new_same;
                default:
                    return relative_position::new_same;
            }
        default:
            return relative_position::new_same;
    }
}


orient
robofat2::maps::rotate(orient orig, rotation rot) {
    switch (orig) {
        case orient::north:
            return rot == rotation::left ? orient::west : orient::east;
        case orient::south:
            return rot == rotation::left ? orient::east : orient::west;
        case orient::east:
            return rot == rotation::left ? orient::north : orient::south;
        case orient::west:
            return rot == rotation::left ? orient::south : orient::north;
        default:
            return orig;
    }
}

orient
robofat2::maps::rotate(orient orig, rotation rot, int amount) {
    amount = amount % 4;
    if (rot == rotation::left) {
        amount = -amount;
    } else if (rot == rotation::right) {
        amount = +amount;
    }

    if (amount == 2 || amount == -2) {
        switch (orig) {
            case orient::north:
                return orient::south;
            case orient::south:
                return orient::north;
            case orient::east:
                return orient::west;
            case orient::west:
                return orient::east;
        }
    } else if (amount == 1 || amount == -3) {
        switch (orig) {
            case orient::north:
                return orient::east;
            case orient::south:
                return orient::west;
            case orient::east:
                return orient::south;
            case orient::west:
                return orient::north;
        }
    } else if (amount == 3 || amount == -1) {
        switch (orig) {
            case orient::north:
                return orient::west;
            case orient::south:
                return orient::east;
            case orient::east:
                return orient::north;
            case orient::west:
                return orient::south;
        }
    }
    /* if (amount == 0) */
    return orig;
}

std::string
robofat2::maps::orient_as_string(orient direction) {
    switch (direction) {
        case orient::north:
            return "orient::north";
        case orient::south:
            return "orient::south";
        case orient::east:
            return "orient::east";
        case orient::west:
            return "orient::west";
        default:
            return "<unknown>";
    }
}
