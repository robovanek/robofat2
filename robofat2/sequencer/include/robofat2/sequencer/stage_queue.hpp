#ifndef ROBOFAT2_SEQUENCER_STAGE_QUEUE_HPP
#define ROBOFAT2_SEQUENCER_STAGE_QUEUE_HPP

#include <robofat2/sequencer/stage.hpp>
#include <string>
#include <list>
#include <memory>

namespace robofat2 {
    namespace sequencer {

        /**
         * Stage sequence.
         * @tparam ContextT Type of the context to pass through.
         */
        template<typename ContextT>
        class stage_queue : public stage<ContextT> {
        public:
            /**
             * Create a new stage queue / sequence.
             * @param ctx Context to pass through.
             * @param name Stage name.
             */
            stage_queue(ContextT &ctx, std::string name)
                    : stage<ContextT>(ctx, std::move(name)),
                      m_queue() {}

            /**
             * Loop on the topmost stages until one of the stages requests a second run.
             * @return Whether everything has finished or not.
             */
            loop operator()() override {
                loop action = loop::exit;
                while (action == loop::exit && !m_queue.empty()) {
                    action = call_front();

                    if (action == loop::exit) {
                        std::unique_ptr<stage<ContextT>> follower = m_queue.front()->acquire_follower();
                        m_queue.pop_front();
                        if (follower) {
                            m_queue.push_front(std::move(follower));
                        }
                    }
                }
                return m_queue.empty() ? loop::exit : loop::again;
            }

            /**
             * Append a stage to this queue.
             * @tparam T Stage type.
             * @tparam Args Types of the stage constructor arguments.
             * @param args Values of the stage constructor arguments.
             * @return This stage queue.
             */
            template<typename T, typename... Args>
            stage_queue *next(Args &&...args) {
                m_queue.emplace_back(new T(this->ctx, std::forward<Args>(args)...));
                return this;
            }

        private:
            /**
             * Call the current stage and announce it if it hasn't been announced yet.
             * @return Stage finish state.
             */
            loop call_front() {
                stage<ContextT> &now = *m_queue.front();
                if (!now.was_announced()) {
                    this->ctx.announce_stage(now);
                    now.mark_announced();
                }
                return now();
            }

            /**
             * List of the child stages.
             */
            std::list<std::unique_ptr<stage < ContextT> > >
            m_queue;
        };
    }
}

#endif //ROBOFAT2_SEQUENCER_STAGE_QUEUE_HPP
