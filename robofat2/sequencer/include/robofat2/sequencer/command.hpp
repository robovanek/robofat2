#ifndef ROBOFAT2_SEQUENCER_COMMAND_HPP
#define ROBOFAT2_SEQUENCER_COMMAND_HPP

#include <robofat2/sequencer/stage.hpp>
#include <functional>

namespace robofat2 {
    namespace sequencer {
        /**
         * Oneshot stage for simple things.
         *
         * The only thing this stage will do is that it will call the provided worker functor
         * and then it will exit.
         *
         * @tparam ContextT Type of the context to pass through.
         */
        template<typename ContextT>
        class command : public stage<ContextT> {
        public:
            /**
             * Create a new command stage.
             * @tparam FunctorT Type of the worker functor.
             * @param ctx Context to pass through.
             * @param fn Worker functor.
             * @param name Stage name.
             */
            template<typename FunctorT>
            command(ContextT &ctx, FunctorT &&fn, std::string name)
                    : stage<ContextT>(ctx, std::move(name)),
                      m_function(std::forward<FunctorT>(fn)) {}

            /**
             * Run the worker functor and exit.
             * @return loop::exit
             */
            loop operator()() final {
                m_function(this->ctx);
                return loop::exit;
            }

        private:
            /**
             * Worker functor.
             */
            std::function<void(ContextT &)> m_function;
        };
    }
}

#endif //ROBOFAT2_SEQUENCER_COMMAND_HPP
