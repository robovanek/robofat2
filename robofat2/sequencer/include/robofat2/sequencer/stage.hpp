#ifndef ROBOFAT2_SEQUENCER_STAGE_HPP
#define ROBOFAT2_SEQUENCER_STAGE_HPP

#include <string>
#include <memory>

namespace robofat2 {
    namespace sequencer {
        /**
         * Completion status
         */
        enum class loop : bool {
            /**
             * Task finished
             */
                    exit = false,
            /**
             * Loop again
             */
                    again = true
        };

        /**
         * Stage of <something> that can be sequenced or nested.
         *
         * @tparam ContextT Context type to be passed to child stages.
         */
        template<typename ContextT>
        class stage {
        public:
            typedef ContextT context_type;
            typedef context_type &context_reference;

            /**
             * Create a new stage.
             * @param ctx Context to passthrough.
             * @param name Stage name.
             */
            explicit stage(ContextT &ctx, std::string name)
                    : ctx(ctx),
                      m_name(std::move(name)) {}

            /**
             * Destroy this stage.
             */
            virtual ~stage() = default;

            /**
             * Perform some work and return the completion state.
             * @return Whether to go to next stage or not.
             */
            virtual loop operator()() = 0;

            /**
             * Get the stage name.
             * @return Name specified in the stage constructor.
             */
            const std::string &name() const {
                return m_name;
            }

            /**
             * Follow this stage with another after this one finishes.
             *
             * @tparam T Type of the following stage.
             * @tparam Args Types of the arguments for the stage constructor.
             * @param args Argument values for the stage constructor.
             * @return loop::exit.
             */
            template<typename T, typename... Args>
            loop follow_with(Args &&...args) {
                m_follower.reset(new T(ctx, std::forward<Args>(args)...));
                return loop::exit;
            }

            /**
             * Take ownership of the follower that may have been set by the task.
             * @return Owning pointer to the follower.
             */
            std::unique_ptr<stage> acquire_follower() {
                return std::move(m_follower);
            }

            /**
             * Check whether user was already informed about this task.
             * @return True if yes, false if not.
             */
            bool was_announced() const {
                return m_announced;
            }

            /**
             * Mark this stage as already announced to the user.
             */
            void mark_announced() {
                m_announced = true;
            }

        protected:
            /**
             * Context to pass through.
             */
            ContextT &ctx;

        private:
            /**
             * Stage name.
             */
            std::string m_name;
            /**
             * Owning pointer to an optional follower stage (it will be scheduled after this one finishes).
             */
            std::unique_ptr<stage> m_follower;
            /**
             * Flag for storing whether this stage was announced to the user.
             */
            bool m_announced = false;
        };
    }
}

#endif //ROBOFAT2_SEQUENCER_STAGE_HPP
