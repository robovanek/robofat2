#include <robofat2/settings/types.hpp>
#include <robofat2/settings/conversions.hpp>

const char *
robofat2::settings::type_to_name(type t) {
    switch (t) {
        default:
            return "invalid";
        case type::boolean:
            return "boolean";
        case type::integer:
            return "integer";
        case type::floating:
            return "floating";
        case type::string:
            return "string";
    }
}

robofat2::settings::type
robofat2::settings::type_from_name(std::string name) {
    std::transform(name.begin(), name.end(), name.begin(), ::tolower);
    if (name == "boolean" || name == "bool") {
        return type::boolean;
    } else if (name == "integer" || name == "int") {
        return type::integer;
    } else if (name == "floating" || name == "float" || name == "flt") {
        return type::floating;
    } else if (name == "string" || name == "str") {
        return type::string;
    } else {
        return type::invalid;
    }
}