#include <robofat2/settings/settings.hpp>
#include <robofat2/settings/settings-impl.hpp>
#include <robofat2/settings/conversions.hpp>

#include <fstream>
#include <utility>
#include <robofat2/utilities/strings.hpp>

using namespace robofat2::settings;

settings::settings() = default;

settings::settings(const std::string &path) {
    load_from_file(path);
}

string
settings::get_raw(const std::string &id, type type) const {
    auto iter = m_values.find(id);
    if (iter == m_values.end()) {
        throw std::runtime_error("settings: cannot find property with name " + id + " and type " + type_to_name(type));
    } else {
        return m_values.at(id);
    }
}

string
settings::get_string(const std::string &id) const {
    return get_typed<string>(id);
}

string settings::get_port(const std::string &id) const {
    std::string value = get_string(id);
    transform(value.begin(), value.end(), value.begin(), ::tolower);

    if (value == "a") {
        return "ev3-ports:outA";
    } else if (value == "b") {
        return "ev3-ports:outB";
    } else if (value == "c") {
        return "ev3-ports:outC";
    } else if (value == "d") {
        return "ev3-ports:outD";

    } else if (value == "s1") {
        return "ev3-ports:in1";
    } else if (value == "s2") {
        return "ev3-ports:in2";
    } else if (value == "s3") {
        return "ev3-ports:in3";
    } else if (value == "s4") {
        return "ev3-ports:in4";
    }
    return value;
}

integer
settings::get_integer(const std::string &id) const {
    return get_typed<integer>(id);
}

floating
settings::get_float(const std::string &id) const {
    return get_typed<floating>(id);
}

boolean
settings::get_bool(const std::string &id) const {
    return get_typed<boolean>(id);
}

type
settings::get_type(const std::string &id) const {
    return m_types.at(id);
}

std::set<std::string>
settings::get_names() const {
    std::set<std::string> result;
    for (auto &&pair : m_values) {
        result.emplace(pair.first);
    }
    return result;
}

void
settings::load_from_file(const std::string &path) {
    m_values.clear();
    m_types.clear();


    std::ifstream in(path.c_str(), std::ios::in);
    std::string line;
    while (std::getline(in, line)) {
        file_line e = file_line::from_string(line);
        if (e.value_type != type::invalid) {
            m_values.emplace(e.name, e.value);
            m_types.emplace(e.name, e.value_type);
        }
    }
}

void
settings::save_to_file(const std::string &path) const {
    std::ofstream out(path.c_str(), std::ios::out | std::ios::trunc);

    for (std::string name : get_names()) {

        type type = get_type(name);
        std::string value = get_raw(name, type);

        out << file_line(type,
                         std::move(name),
                         std::move(value));
    }
}

file_line::file_line() : value_type(type::invalid),
                         name("error"),
                         value("0") {}

file_line::file_line(type value_type,
                     std::string name,
                     std::string value)
        : value_type(value_type),
          name(std::move(name)),
          value(std::move(value)) {}

std::ostream &
robofat2::settings::operator<<(std::ostream &os, const file_line &line) {
    os << type_to_name(line.value_type) << ":\t";
    os << line.name << ":\t";
    os << line.value << std::endl;
    return os;
}

file_line
file_line::from_string(const std::string &line) {
    using namespace robofat2::utilities;

    if (line.length() == 0 || line.at(0) == '#') {
        return file_line{};
    }
    size_t split1 = line.find_first_of(':');
    size_t split2 = line.find_first_of(':', split1 + 1);
    if (split1 == std::string::npos || split2 == std::string::npos) {
        return file_line{};
    } else {
        std::string type = line.substr(0,
                                       split1);
        std::string key = line.substr(split1 + 1,
                                      split2 - split1 - 1);
        std::string value = line.substr(split2 + 1);
        trim(type);
        trim(key);
        trim(value);
        return file_line{type_from_name(type), key, value};
    }
}
