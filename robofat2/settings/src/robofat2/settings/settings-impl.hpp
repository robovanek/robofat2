#ifndef ROBOFAT2_SETTINGS_IMPL_HPP
#define ROBOFAT2_SETTINGS_IMPL_HPP

#include <string>

#include <robofat2/settings/types.hpp>
#include <ostream>

namespace robofat2 {
    namespace settings {
        /**
         * Helper class for parsing settings lines.
         */
        struct file_line {
            /**
             * Initialize an empty invalid line.
             */
            file_line();

            /**
             * Initialize a possible valid line.
             * @param value_type Type of property value.
             * @param name Property name.
             * @param value Property value.
             */
            file_line(type value_type,
                      std::string name,
                      std::string value);

            /**
             * Initialize a property from a configuration file line.
             * @param line Raw line from a configuration file.
             * @return Either a valid or invalid file_line object.
             */
            static file_line
            from_string(const std::string &line);

            /**
             * Write a property to an output stream.
             * @param os Stream to write the property to.
             * @param line Line to write.
             * @return Stream os.
             */
            friend std::ostream &
            operator<<(std::ostream &os,
                       const file_line &line);

            /**
             * Type of the property value.
             */
            type value_type;
            /**
             * Property name/identifier.
             */
            std::string name;
            /**
             * Property value.
             */
            std::string value;
        };

        std::ostream &
        operator<<(std::ostream &os, const file_line &line);
    }
}

#endif //ROBOFAT2_SETTINGS_IMPL_HPP
