#ifndef ROBOFAT2_SETTINGS_TYPES_HPP
#define ROBOFAT2_SETTINGS_TYPES_HPP

#include <string>
#include <cstdint>

namespace robofat2 {
    namespace settings {
        /**
         * C++ type for a string.
         */
        typedef std::string string;
        /**
         * C++ type for a integer number.
         */
        typedef int32_t integer;
        /**
         * C++ type for a decimal number.
         */
        typedef float floating;
        /**
         * C++ type for a boolean value.
         */
        typedef bool boolean;

        /**
         * Enumeration of possible value types.
         */
        enum class type {
            /**
             * Not a type; rather a placeholder for a parse-error or unspecified type.
             */
            invalid,
            /**
             * Boolean (yes/no) value type.
             */
            boolean,
            /**
             * Integer (-1, 0, 1, 2) value type.
             */
            integer,
            /**
             * Decimal (0.1, -100.2) value type.
             */
            floating,
            /**
             * String (hello) value type.
             */
            string
        };
    }
}

#endif //ROBOFAT2_SETTINGS_TYPES_HPP
