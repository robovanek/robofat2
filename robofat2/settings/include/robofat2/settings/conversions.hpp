#ifndef ROBOFAT2_SETTINGS_CONVERSIONS_HPP
#define ROBOFAT2_SETTINGS_CONVERSIONS_HPP

#include <algorithm>
#include <stdexcept>

#include <robofat2/settings/types.hpp>

namespace robofat2 {
    namespace settings {
        template<typename T>
        struct convertor {
        };

        template<>
        struct convertor<string> {
            typedef string assoc_type;

            static assoc_type load(const std::string &data) {
                return data;
            }

            static std::string save(const assoc_type &data) {
                return data;
            }
        };

        template<>
        struct convertor<integer> {
            typedef integer assoc_type;

            static assoc_type load(const std::string &data) {
                return std::stoi(data, nullptr, 10);
            }

            static std::string save(assoc_type data) {
                return std::to_string(data);
            }
        };

        template<>
        struct convertor<floating> {
            typedef floating assoc_type;

            static assoc_type load(const std::string &data) {
                return std::stof(data);
            }

            static std::string save(assoc_type data) {
                return std::to_string(data);
            }
        };

        template<>
        struct convertor<boolean> {
            typedef boolean assoc_type;

            static assoc_type load(std::string data) {
                std::transform(data.begin(), data.end(), data.begin(), ::tolower);
                if (data == "false" || data == "0" || data == "off" || data == "no" || data == "n")
                    return false;
                if (data == "true" || data == "1" || data == "on" || data == "yes" || data == "y")
                    return true;
                throw std::runtime_error("Invalid bool format: " + data);
            }

            static std::string save(assoc_type data) {
                return data ? "true" : "false";
            }
        };

        template<type T>
        struct type_of_enum {
        };

        template<>
        struct type_of_enum<type::string> {
            typedef string type;
        };

        template<>
        struct type_of_enum<type::integer> {
            typedef integer type;
        };

        template<>
        struct type_of_enum<type::floating> {
            typedef floating type;
        };

        template<>
        struct type_of_enum<type::boolean> {
            typedef boolean type;
        };

        template<typename T>
        struct enum_of_type {
        };

        template<>
        struct enum_of_type<string> {
            static constexpr type value = type::string;
        };

        template<>
        struct enum_of_type<integer> {
            static constexpr type value = type::integer;
        };

        template<>
        struct enum_of_type<floating> {
            static constexpr type value = type::floating;
        };

        template<>
        struct enum_of_type<boolean> {
            static constexpr type value = type::boolean;
        };

        extern const char *type_to_name(type t);

        extern type type_from_name(std::string name);
    }
}

#endif //ROBOFAT2_SETTINGS_CONVERSIONS_HPP
