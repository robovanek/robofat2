#ifndef ROBOFAT2_SETTINGS_SETTINGS_HPP
#define ROBOFAT2_SETTINGS_SETTINGS_HPP

#include <map>
#include <set>
#include <chrono>

#include <robofat2/settings/types.hpp>
#include <robofat2/settings/conversions.hpp>

namespace robofat2 {
    namespace settings {
        /**
         * Class for loading a set of properties from a file.
         *
         * The format of a file is a set of type-name-value triples:
         *
         * type1: name1: value1
         * type2: name2: value2
         *
         * Additionally, a line may start with a comment like this:
         * # hello
         *
         * However, the hash character has to be the first character on the line.
         *
         * There are four value types {@see robofat2::settings::type}:
         *   - boolean  (alt. bool)  - a yes/no type mapped on bool,
         *   - integer  (alt. int)   - an integer (0, 1, 2, ...) type mapped on int32_t,
         *   - floating (alt. float) - a real integer type (0.1, ...) mapped on float,
         *   - string   (alt. str)   - a string that can contain any single-line text data.
         */
        class settings {
        public:
            /**
             * Initialize an empty settings instance.
             */
            explicit settings();

            /**
             * Initialize a settings instance from a file.
             * @param path Path to a configuration file.
             */
            explicit settings(const std::string &path);

            /**
             * Try to get a string value from settings.
             *
             * @param name Property name.
             * @throws std::runtime_error when the property does not exist.
             * @throws std::runtime_error when the real and expected types don't match.
             * @return Property value.
             */
            string get_string(const std::string &id) const;

            /**
             * Try to get a port value from settings.
             *
             * @param name Property name.
             * @throws std::runtime_error when the property does not exist.
             * @throws std::runtime_error when the real and expected types don't match.
             * @return Property value.
             */
            string get_port(const std::string &id) const;

            /**
             * Try to get a integer value from settings.
             *
             * @param name Property name.
             * @throws std::runtime_error when the property does not exist.
             * @throws std::runtime_error when the real and expected types don't match.
             * @return Property value.
             */
            integer get_integer(const std::string &id) const;

            /**
             * Try to get a decimal value from settings.
             *
             * @param name Property name.
             * @throws std::runtime_error when the property does not exist.
             * @throws std::runtime_error when the real and expected types don't match.
             * @return Property value.
             */
            floating get_float(const std::string &id) const;

            /**
             * Try to get a boolean value from settings.
             *
             * @param name Property name.
             * @throws std::runtime_error when the property does not exist.
             * @throws std::runtime_error when the real and expected types don't match.
             * @return Property value.
             */
            boolean get_bool(const std::string &id) const;

            /**
             * Try to get a duration value from settings.
             *
             * The underlying property must have integer type and must be specified in milliseconds.
             *
             * @tparam T std::chrono::duration type to cast the result to.
             * @param name Property name.
             * @throws std::runtime_error when the property does not exist.
             * @throws std::runtime_error when the real and expected types don't match.
             * @return Property value.
             */
            template<typename T = std::chrono::milliseconds>
            T get_duration(const std::string &name) const;

            /**
             * Try to find a type of a property.
             *
             * @param id Property name.
             * @throws std::runtime_error if the property does not exist
             * @return Current type of the property.
             */
            type get_type(const std::string &id) const;

            /**
             * Try to get raw property value from settings.
             *
             * @param id Property name.
             * @param type Expected property type (not checked, only used for error handling).
             * @throws std::runtime_error when the property does not exist.
             * @return Raw property value.
             */
            string get_raw(const std::string &id, type type = type::invalid) const;

            /**
             * Enumerate available properties.
             * @return Set of available property names.
             */
            std::set<std::string> get_names() const;

            /**
             * Store settings to a file.
             * @param path Path to the configuration file.
             */
            void save_to_file(const std::string &path) const;

            /**
             * Load settings from a file.
             * @param path Path to the configuration file.
             */
            void load_from_file(const std::string &path);

        private:
            /**
             * Try to get a property value from settings and convert it to the correct type.
             *
             * @tparam T C++ type of the property.
             * @param id Name of the property.
             * @throws std::runtime_error when the property does not exist.
             * @throws std::runtime_error when the real and expected types don't match.
             * @return Cast property value.
             */
            template<typename T>
            T get_typed(const std::string &id) const;

            /**
             * Mapping from property name to its value.
             */
            std::map<std::string, string> m_values;

            /**
             * Mapping from property name to its type.
             */
            std::map<std::string, type> m_types;
        };

        template<typename T>
        T settings::get_typed(const std::string &id) const {
            constexpr type expected = enum_of_type<T>::value;

            string raw = get_raw(id, expected);
            type real = get_type(id);

            if (real != expected) {
                throw std::runtime_error("settings: property with name " + id + " has invalid type. " +
                                         "Expected: " + type_to_name(expected) + ", got: " + type_to_name(real));
            }
            try {
                return convertor<T>::load(raw);
            } catch (...) {
                std::throw_with_nested(std::runtime_error(
                                          "settings: property with name '" + id +
                                          "', type '" + type_to_name(expected) +
                                          "' and value '" + raw + "' cannot be cast"));
            }
        }

        template<typename T>
        T settings::get_duration(const std::string &name) const {
            integer millisCount = get_integer(name);
            std::chrono::milliseconds millisDuration(millisCount);

            return std::chrono::duration_cast<T>(millisDuration);
        }
    }
}

#endif //ROBOFAT2_SETTINGS_SETTINGS_HPP
