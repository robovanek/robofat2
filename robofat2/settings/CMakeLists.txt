cmake_minimum_required(VERSION 3.7)

project("RoboFAT2-Settings" LANGUAGES CXX)

add_library(robosettings STATIC
        include/robofat2/settings/types.hpp
        include/robofat2/settings/conversions.hpp
        include/robofat2/settings/settings.hpp
        src/robofat2/settings/conversions.cpp
        src/robofat2/settings/settings.cpp
        src/robofat2/settings/settings-impl.hpp
        )

target_include_directories(robosettings
        PUBLIC
        $<INSTALL_INTERFACE:include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src)

target_link_libraries(robosettings
        PRIVATE
        RoboFAT2::utilities
        )

set_target_properties(robosettings PROPERTIES
        CXX_STANDARD 11
        CXX_STANDARD_REQUIRED YES
        CXX_EXTENSIONS NO)

add_library(RoboFAT2::settings ALIAS robosettings)
