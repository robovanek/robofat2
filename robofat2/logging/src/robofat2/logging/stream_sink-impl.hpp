#ifndef ROBOFAT2_STREAM_SINK_IMPL_HPP
#define ROBOFAT2_STREAM_SINK_IMPL_HPP

#include <robofat2/logging/stream_sink.hpp>

namespace robofat2 {
    namespace logging {
        /**
         * Non-owning ostream holder.
         *
         * It just holds a raw pointer and does not delete it on its destruction.
         */
        class non_owning_holder : public ostream_holder {
        public:
            /**
             * Initialize a new reference holder.
             * @param stream Reference to the ostream to wrap.
             */
            explicit non_owning_holder(std::ostream &stream);

            /**
             * @inherit
             */
            std::ostream &operator*() final;

            /**
             * Factory method for this holder.
             * @param stream Plain reference to an ostream object.
             * @return Pointer useful for holder storage.
             */
            static std::unique_ptr<ostream_holder> make(std::ostream &stream);

        private:
            /**
             * Non-managed pointer to the underlying ostream.
             */
            std::ostream *m_stream;
        };

        /**
         * Owning ostream holder.
         *
         * It holds a smart pointer and it deletes the stream on the holder's destruction.
         */
        class owning_holder : public ostream_holder {
        public:
            /**
             * Initialize a new owning ostream holder.
             * @param stream Managed pointer to a stream.
             */
            explicit owning_holder(std::unique_ptr<std::ostream> stream);

            /**
             * @inherit
             */
            std::ostream &operator*() final;

            /**
             * Factory method for this holder.
             * @param stream Managed reference to an ostream object.
             * @return Pointer useful for holder storage.
             */
            static std::unique_ptr<ostream_holder> make(std::unique_ptr<std::ostream> stream);

        private:
            /**
             * Managed pointer to the underlying ostream.
             */
            std::unique_ptr<std::ostream> m_stream;
        };
    }
}

#endif //ROBOFAT2_STREAM_SINK_IMPL_HPP
