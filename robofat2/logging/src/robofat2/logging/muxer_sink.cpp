#include <robofat2/logging/muxer_sink.hpp>

using namespace robofat2::logging;

muxer_sink::sink_id muxer_sink::add_sink(sink_ptr sink) {
    if (sink) {
        int id = m_nextID;
        m_nextID++;
        m_children.emplace(id, std::move(sink));
        return id;
    } else {
        throw std::out_of_range("Cannot add nullptr sink to muxer_sink");
    }
}

void muxer_sink::remove_sink(muxer_sink::sink_id ID) {
    m_children.erase(ID);
}

void muxer_sink::log(severity level, const std::string &component, const std::string &text) {
    for (const auto &pair : m_children) {
        pair.second->log(level, component, text);
    }
}

severity muxer_sink::get_verbosity() const {
    severity level = severity_fatal;
    for (const auto &pair : m_children) {
        level = std::min(level, pair.second->get_verbosity());
    }
    return level;
}

sink_ptr muxer_sink::get_sink(sink_id ID) {
    return m_children.at(ID);
}
