#include <robofat2/logging/stream_sink.hpp>
#include <robofat2/logging/stream_sink-impl.hpp>
#include <ostream>
#include <sstream>

using namespace robofat2::logging;

stream_sink::stream_sink(std::ostream &output, severity level)
        : m_minimal_level(level),
          m_initial_time(clock::now()),
          m_output(non_owning_holder::make(output)),
          m_formatter(simple_formatter::make()) {}

stream_sink::stream_sink(std::unique_ptr<std::ostream> output, severity level)
        : m_minimal_level(level),
          m_initial_time(clock::now()),
          m_output(owning_holder::make(std::move(output))),
          m_formatter(simple_formatter::make()) {}

void
stream_sink::log(severity level,
                 const std::string &component,
                 const std::string &text) {

    if (level >= m_minimal_level) {
        **m_output << m_formatter->format(level, component, text, clock::now() - m_initial_time);
    }
}

void stream_sink::set_formatter(formatter_ptr formatter) {
    if (formatter) {
        m_formatter = std::move(formatter);
    } else {
        throw std::out_of_range("Cannot set formatter to nullptr.");
    }
}

non_owning_holder::non_owning_holder(std::ostream &stream)
        : m_stream(&stream) {}

std::ostream &non_owning_holder::operator*() {
    return *m_stream;
}

std::unique_ptr<ostream_holder> non_owning_holder::make(std::ostream &stream) {
    return std::unique_ptr<ostream_holder>(new non_owning_holder(stream));
}

owning_holder::owning_holder(std::unique_ptr<std::ostream> stream)
        : m_stream(std::move(stream)) {}

std::ostream &owning_holder::operator*() {
    return *m_stream;
}

std::unique_ptr<ostream_holder> owning_holder::make(std::unique_ptr<std::ostream> stream) {
    return std::unique_ptr<ostream_holder>(new owning_holder(std::move(stream)));
}
