#include <cerrno>
#include <cstring>
#include <sstream>

#include <robofat2/logging/source.hpp>
#include <robofat2/logging/sink.hpp>
#include <robofat2/logging/stderr_sink.hpp>
#include <robofat2/logging/discard_sink.hpp>

using namespace robofat2::logging;

source::source(std::string name,
               std::shared_ptr<sink> backend)
        : m_component(std::move(name)),
          m_backend(std::move(backend)) {
}

void
source::debug(const std::string &text) {
    if (m_backend) {
        return m_backend->log(severity_debug, m_component, text);
    }
}

void
source::info(const std::string &text) {
    if (m_backend) {
        return m_backend->log(severity_info, m_component, text);
    }
}

void
source::warn(const std::string &text) {
    if (m_backend) {
        return m_backend->log(severity_warn, m_component, text);
    }
}

void
source::error(const std::string &text) {
    if (m_backend) {
        return m_backend->log(severity_error, m_component, text);
    }
}

void
source::fatal(const std::string &text) {
    if (m_backend) {
        return m_backend->log(severity_fatal, m_component, text);
    }
}

void
source::debug(const char *format, ...) {
    if (can_log(severity_debug)) {
        va_list va;
        va_start(va, format);
        debug(vformatn(format, va));
        va_end(va);
    }
}

void
source::info(const char *format, ...) {
    if (can_log(severity_info)) {
        va_list va;
        va_start(va, format);
        info(vformatn(format, va));
        va_end(va);
    }
}

void
source::warn(const char *format, ...) {
    if (can_log(severity_warn)) {
        va_list va;
        va_start(va, format);
        warn(vformatn(format, va));
        va_end(va);
    }
}

void
source::error(const char *format, ...) {
    if (can_log(severity_error)) {
        va_list va;
        va_start(va, format);
        error(vformatn(format, va));
        va_end(va);
    }
}

void
source::fatal(const char *format, ...) {
    if (can_log(severity_fatal)) {
        va_list va;
        va_start(va, format);
        fatal(vformatn(format, va));
        va_end(va);
    }
}

bool
source::can_log(severity level) const {
    if (m_backend) {
        return level >= m_backend->get_verbosity();
    } else {
        return false;
    }
}

void
source::log_errno(severity level,
                  const std::string &call,
                  const std::string &context) {
    return log_errno(level, errno, call, context);
}

void
source::log_errno(severity level,
                  int errno_value,
                  const std::string &call,
                  const std::string &context) {
    if (can_log(level) && m_backend) {
        m_backend->log(level, m_component, get_errno_message(errno_value, call, context));
    }
}

source source::make_stderr(std::string name) {
    return source(std::move(name), stderr_sink::make());
}

source source::make_discard() {
    return source("<empty>", discard_sink::make());
}

std::string
robofat2::logging::get_errno_message(int errno_value,
                                     const std::string &call,
                                     const std::string &context) {
    std::ostringstream str;
    str << "IO error: ";
    str << "Cannot do " << call << " on '" << context << "':";
    str << " errno=" << errno_value << " [" << strerror(errno_value) << "].";
    return str.str();
}
