
#include <robofat2/logging/stderr_sink.hpp>
#include <iostream>

using namespace robofat2::logging;

stderr_sink::stderr_sink(severity level)
        : stream_sink(std::cerr, level) {}
