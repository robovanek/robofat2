#include <robofat2/logging/common.hpp>

std::string robofat2::logging::formatn(const char *format, ...) {
    va_list va;
    va_start(va, format);
    std::string result = vformatn(format, va);
    va_end(va);
    return result;
}

std::string robofat2::logging::vformatn(const char *format, va_list va) {
    va_list args1, args2;
    va_copy(args1, va);
    va_copy(args2, va);

    int bytes = std::vsnprintf(nullptr, 0, format, args1) + 1;
    if (bytes < 0) {
        va_end(args2);
        va_end(args1);
        return "std::vsnprintf returned negative value";
    }
    va_end(args1);

    std::string result(bytes, 0);
    std::vsnprintf(&result.front(), result.size(), format, args2);
    result.erase(result.size() - 1);
    va_end(args2);

    return result;
}