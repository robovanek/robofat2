#include <robofat2/logging/formatter.hpp>
#include <sstream>

using namespace robofat2::logging;

std::string
simple_formatter::format(severity level,
                         const std::string &component,
                         const std::string &text,
                         const elapsed_time &sinceT0) {
    std::ostringstream ost;
    ost << get_time_string(sinceT0) << " | ";
    ost << get_severity_string(level) << " | ";
    ost << component << ": ";
    ost << text << std::endl;
    return ost.str();
}


std::string
simple_formatter::get_time_string(const elapsed_time &deltaNS) {
    float number = deltaNS.count() / 1000000000.0f;
    return formatn("%6.2f", number);
}

const char *
simple_formatter::get_severity_string(severity level) {
    switch (level) {
        case severity_debug:
            return "debug";
        case severity_info:
            return "info ";
        case severity_warn:
            return "warn ";
        case severity_error:
            return "error";
        case severity_fatal:
            return "fatal";
        default:
            throw std::runtime_error("unknown severity level: " + std::to_string((int) level));
    }
}

formatter_ptr simple_formatter::make() {
    return robofat2::logging::formatter_ptr(new simple_formatter);
}
