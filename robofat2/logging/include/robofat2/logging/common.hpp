#ifndef ROBOFAT2_LOG_COMMON_HPP
#define ROBOFAT2_LOG_COMMON_HPP

#include <string>
#include <cstdarg>

namespace robofat2 {
    namespace logging {
        /**
         * Logging output severity.
         */
        enum severity {
            /**
             * Debugging message - not useful except for hunting down a problem.
             */
                    severity_debug,
            /**
             * Info message - describes a noteworthy change in program state.
             */
                    severity_info,
            /**
             * Warning message - describes a problem that is not very important.
             */
                    severity_warn,
            /**
             * Error message - describes a problem that is important.
             */
                    severity_error,
            /**
             * Fatal error message - describes a problem after which the program is unable to continue.
             */
                    severity_fatal
        };

        /**
         * snprintf() variant returning a std::string.
         *
         * This will likely be implemented using vformatn() / vsnprintf().
         *
         * @param format Printf format string.
         * @param ... Printf parameters.
         * @return String formatted according to the format string.
         */
        std::string formatn(const char *format, ...) __attribute__((__format__ (__printf__, 1, 2)));

        /**
         * vsnprintf() variant returning a std::string.
         * @param format Printf format string.
         * @param va Printf parameters as a varargs list.
         * @return String formatted according to the format string.
         */
        std::string vformatn(const char *format, va_list va);
    }
}

#endif //ROBOFAT2_LOG_COMMON_HPP
