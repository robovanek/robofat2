#ifndef ROBOFAT2_STDERR_SINK_HPP
#define ROBOFAT2_STDERR_SINK_HPP

#include <chrono>
#include <robofat2/logging/stream_sink.hpp>

namespace robofat2 {
    namespace logging {
        /**
         * Simple logger for stderr.
         */
        class stderr_sink : public stream_sink {
        public:
            /**
             * Initialize this logger.
             *
             * The initial time (t=0s) will be set to the time of the execution of this constructor.
             *
             * @param level Level of the logging output that will be printed out.
             */
            explicit stderr_sink(severity level = severity_info);

            /**
             * Initialize a new instance of this logging sink
             */
            static sink_ptr make() {
                return std::make_shared<stderr_sink>();
            }
        };
    }
}

#endif //ROBOFAT2_STDERR_SINK_HPP
