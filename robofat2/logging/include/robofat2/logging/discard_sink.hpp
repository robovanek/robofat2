#ifndef ROBOFAT2_DISCARD_SINK_HPP
#define ROBOFAT2_DISCARD_SINK_HPP

#include <robofat2/logging/sink.hpp>

namespace robofat2 {
    namespace logging {
        /**
         * Logger that will discard all incoming messages, i.e. a "black hole".
         */
        class discard_sink : public sink {
        public:
            /**
             * Initialize this logger.
             */
            discard_sink() = default;

            /**
             * Noop for this logger.
             */
            void log(severity level,
                     const std::string &component,
                     const std::string &text) final {}

            /**
             * Always only the highest priority for this logger.
             */
            severity get_verbosity() const final {
                return severity_fatal;
            }

            /**
             * Initialize a new instance of this logging sink
             */
            static sink_ptr make() {
                return std::make_shared<discard_sink>();
            }
        };
    }
}


#endif //ROBOFAT2_DISCARD_SINK_HPP
