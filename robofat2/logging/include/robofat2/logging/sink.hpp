#ifndef ROBOFAT2_LOG_SINK_HPP
#define ROBOFAT2_LOG_SINK_HPP

#include <string>
#include <memory>
#include <robofat2/logging/common.hpp>

namespace robofat2 {
    namespace logging {
        /**
         * Generic interface for consuming logging output.
         */
        class sink {
        public:
            /**
             * Destroy this logger.
             */
            virtual ~sink() = default;

            /**
             * Add a message to the log.
             * @param level How severe is the situation described by the logging message.
             * @param component Which component does the logging output come from.
             * @param text Contents of the logging message.
             */
            virtual void log(severity level,
                             const std::string &component,
                             const std::string &text) = 0;

            /**
             * Get which severities will be logged.
             * @return The lowest verbosity level that is currently accepted.
             */
            virtual severity get_verbosity() const = 0;
        };

        /**
         * Helper for referring to a generic sink implementation
         */
        using sink_ptr = std::shared_ptr<sink>;
    }
}

#endif //ROBOFAT2_LOG_SINK_HPP
