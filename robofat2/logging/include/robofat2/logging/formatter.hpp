#ifndef ROBOFAT2_FORMATTER_HPP
#define ROBOFAT2_FORMATTER_HPP

#include <memory>
#include <chrono>
#include <robofat2/logging/common.hpp>

namespace robofat2 {
    namespace logging {
        /**
         * Class for formatting logging messages & meta to final output string.
         */
        class formatter {
        public:
            /**
             * Type used for time elapsed since T0 (started logging)
             */
            typedef std::chrono::nanoseconds elapsed_time;

            /**
             * Destroy this formatter instance.
             */
            virtual ~formatter() = default;

            /**
             * Format the logging message line.
             * @param level Message severity.
             * @param component From where the message comes.
             * @param text Content of the message.
             * @param time From when the message originates.
             * @return String to print to stderr.
             */
            virtual std::string format(severity level,
                                       const std::string &component,
                                       const std::string &text,
                                       const elapsed_time &sinceT0) = 0;
        };

        /**
         * Pointer for storing formatter implementations.
         */
        using formatter_ptr = std::unique_ptr<formatter>;

        /**
         * Default simple formatter.
         */
        class simple_formatter : public formatter {
        public:
            /**
             * @inherit
             */
            std::string format(severity level,
                               const std::string &component,
                               const std::string &text,
                               const elapsed_time &sinceT0) final;

            /**
             * Factory method for this formatter.
             * @return Pointer suitable for storage of this formatter.
             */
            static formatter_ptr make();

        protected:
            /**
             * Get a string describing a time instant.
             * @param when Which time point to describe.
             * @return Decimal point number containing number of seconds since the construction of this logger.
             */
            virtual std::string get_time_string(const elapsed_time &deltaNS);

            /**
             * Get a string describing a severity level.
             * @param level Level to decribe.
             * @return Suitably padded short string identifying the provided severity level.
             */
            virtual const char *get_severity_string(severity level);
        };
    }
}

#endif //ROBOFAT2_FORMATTER_HPP
