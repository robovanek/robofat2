#ifndef ROBOFAT2_STREAM_SINK_HPP
#define ROBOFAT2_STREAM_SINK_HPP

#include <chrono>
#include <ostream>
#include <robofat2/logging/sink.hpp>
#include <robofat2/logging/formatter.hpp>

namespace robofat2 {
    namespace logging {
        /**
         * Wrapper for holding both owning and non-owning references to std::ostream.
         *
         * Note: this is an example of type erasure -- this is a definition of a common
         * interface that all type adapters must have. The stream sink can then just
         * have a pointer to this common superclass and it does not have to care
         * whether the underlying reference is owning or not.
         */
        class ostream_holder {
        public:
            /**
             * Destroy this reference holder implementation.
             */
            virtual ~ostream_holder() = default;

            /**
             * Get a reference to the std::ostream that this holder has.
             * @return Reference to ostream.
             */
            virtual std::ostream &operator*() = 0;
        };

        /**
         * Simple logger for streams.
         */
        class stream_sink : public sink {
            using clock = std::chrono::system_clock;
            using duration = clock::duration;
            using time_point = clock::time_point;

        public:
            /**
             * Initialize this logger.
             *
             * The initial time (t=0s) will be set to the time of the execution of this constructor.
             *
             * This constructor will not own the std::ostream reference;
             * object destruction & lifetime must be handled separately.
             *
             * @param output Target stream for logging.
             * @param level Level of the logging output that will be printed out.
             */
            explicit stream_sink(std::ostream &output, severity level = severity_info);

            /**
             * Initialize this logger.
             *
             * The initial time (t=0s) will be set to the time of the execution of this constructor.
             *
             * This constructor will own the std::ostream reference. This means that the
             * underlying system resources will be cleaned after the use.
             *
             * @param output Smart pointer holding the ostream reference.
             * @param level Level of the logging output that will be printed out.
             */
            explicit stream_sink(std::unique_ptr<std::ostream> output, severity level = severity_info);

            /**
             * @inherit
             */
            void log(severity level,
                     const std::string &component,
                     const std::string &text) override;

            /**
             * Replace the default formatter with the specified one.
             * @param formatter Formatter to use.
             */
            void set_formatter(formatter_ptr formatter);

            /**
             * @inherit
             */
            severity get_verbosity() const final {
                return m_minimal_level;
            }

            /**
             * Set which severities will be logged.
             * @param minimum The lowest verbosity level that will be accepted.
             */
            void set_verbosity(severity minimum) {
                m_minimal_level = minimum;
            }

            /**
             * Initialize a new instance of this logging sink
             */
            static sink_ptr make(std::ostream &output) {
                return std::make_shared<stream_sink>(output);
            }

        private:
            /**
             * Minimum message importance for pass-through.
             */
            severity m_minimal_level;
            /**
             * Time point relative to which message timestamps are calculated.
             */
            time_point m_initial_time;
            /**
             * Output stream
             */
            std::unique_ptr<ostream_holder> m_output;
            /**
             * Formatter used for output
             */
            formatter_ptr m_formatter;
        };
    }
}

#endif //ROBOFAT2_STREAM_SINK_HPP
