#ifndef ROBOFAT2_MUXER_SINK_HPP
#define ROBOFAT2_MUXER_SINK_HPP

#include <robofat2/logging/sink.hpp>
#include <unordered_map>

namespace robofat2 {
    namespace logging {
        /**
         * Sink for multiplexing logging output to multiple slave sinks.
         */
        class muxer_sink : public sink {
        public:
            /**
             * Slave sink identifier.
             */
            using sink_id = unsigned int;
            /**
             * Pointer to slave sink.
             */
            using slave_sink = sink_ptr;

            /**
             * Initialize new sink multiplexer.
             */
            muxer_sink() = default;

            /**
             * Add a sink to forward logging output to.
             * @param sink Pointer to the sink.
             * @throws std::out_of_range if the sink pointer is equal to nullptr.
             * @return Slave sink ID.
             */
            sink_id add_sink(sink_ptr sink);

            /**
             * Acquire the sink associated to this sink ID.
             * @param ID Slave sink ID.
             * @throws std::out_of_range if such an ID does not exist.
             * @return Pointer to the associated sink.
             */
            sink_ptr get_sink(sink_id ID);

            /**
             * Remove the sink with the specified ID from this muxer.
             * @param ID ID of the slave sink to remove.
             */
            void remove_sink(sink_id ID);

            /**
             * Forward logging output to slave sinks.
             * @param level Level to log at.
             * @param component Component that the log output comes from.
             * @param text Content of the logging message.
             */
            void log(severity level, const std::string &component, const std::string &text) override;

            /**
             * Get the accepted verbosity level.
             * @return Most permissive severity of all of the slave sinks.
             */
            severity get_verbosity() const override;

        private:
            /**
             * Next slave sink ID.
             */
            sink_id m_nextID = 0;
            /**
             * List of slave sinks and their associated IDs.
             */
            std::unordered_map<sink_id, slave_sink> m_children;
        };
    }
}

#endif //ROBOFAT2_MUXER_SINK_HPP
