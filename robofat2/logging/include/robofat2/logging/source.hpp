#ifndef ROBOFAT2_LOG_SOURCE_HPP
#define ROBOFAT2_LOG_SOURCE_HPP

#include <string>
#include <memory>

#include <robofat2/logging/common.hpp>
#include <robofat2/logging/sink.hpp>

namespace robofat2 {
    namespace logging {
        /**
         * Log output provider.
         *
         * This is the class that should be used for logging messages in the user code.
         */
        class source {
        public:
            /**
             * Initialize this logging source.
             *
             * @param name Name of the component that will be using this source.
             * @param backend Where should the messages be routed to.
             */
            source(std::string name, sink_ptr backend);

            /**
             * Add a debug message to the log.
             * @param text Contents of the message.
             */
            void debug(const std::string &text);

            /**
             * Add an info message to the log.
             * @param text Contents of the message.
             */
            void info(const std::string &text);

            /**
             * Add a warning message to the log.
             * @param text Contents of the message.
             */
            void warn(const std::string &text);

            /**
             * Add an error message to the log.
             * @param text Contents of the message.
             */
            void error(const std::string &text);

            /**
             * Add a fatal message to the log.
             * @param text Contents of the message.
             */
            void fatal(const std::string &text);

            /**
             * Add a debug message to the log.
             * @param format Printf format specifier.
             * @param ... Printf parameters.
             */
            void debug(const char *format, ...) __attribute__((__format__ (__printf__, 2, 3)));

            /**
             * Add an info message to the log.
             * @param format Printf format specifier.
             * @param ... Printf parameters.
             */
            void info(const char *format, ...) __attribute__((__format__ (__printf__, 2, 3)));

            /**
             * Add a warning message to the log.
             * @param format Printf format specifier.
             * @param ... Printf parameters.
             */
            void warn(const char *format, ...) __attribute__((__format__ (__printf__, 2, 3)));

            /**
             * Add an error message to the log.
             * @param format Printf format specifier.
             * @param ... Printf parameters.
             */
            void error(const char *format, ...) __attribute__((__format__ (__printf__, 2, 3)));

            /**
             * Add a fatal message to the log.
             * @param format Printf format specifier.
             * @param ... Printf parameters.
             */
            void fatal(const char *format, ...) __attribute__((__format__ (__printf__, 2, 3)));

            /**
             * Add a message describing an errno condition to the log.
             * Errno value will be taken from the environment.
             *
             * @param call Summary of the syscall name & arguments.
             * @param context Summary of the syscall calling context (i.e. filename or similar).
             */
            void log_errno(severity level, const std::string &call, const std::string &context);

            /**
             * Add a message describing an errno condition to the log.
             *
             * @param errno_value Value of the errno variable at the time after the problematic call.
             * @param call Summary of the syscall name & arguments.
             * @param context Summary of the syscall calling context (i.e. filename or similar).
             */
            void log_errno(severity level, int errno_value, const std::string &call, const std::string &context);

            /**
             * Initialize a source routed to the default stderr logger.
             * @param name Logging component name.
             * @return Initialized logging source.
             */
            static source make_stderr(std::string name);

            /**
             * Initialize a source routed to a black hole.
             * @return Initialized logging source.
             */
            static source make_discard();

        private:
            /**
             * Check whether the backend logger will accept the specified severity.
             * Calling this function can be useful to avoid wasting time preparing the message.
             *
             * @param level Level to check.
             *
             * @return True if the logger will take this severity, false otherwise.
             */
            bool can_log(severity level) const;

            /**
             * Name of the associated component.
             */
            std::string m_component;
            /**
             * Logging sink that this log source is attached to.
             */
            sink_ptr m_backend;
        };

        /**
         * Generate a string description of a syscall error.
         * @param errno_value Errno value that was set by the syscall.
         * @param call Syscall name & arguments.
         * @param context Call context (i.e. file path or similar).
         * @return Message to be logged.
         */
        std::string get_errno_message(int errno_value, const std::string &call, const std::string &context);
    }
}

#endif //ROBOFAT2_LOG_SOURCE_HPP
