/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PID implementation.
 */

#include <robofat2/pid/pid.hpp>
#include <robofat2/pid/differentiator.hpp>
#include <robofat2/pid/integrator.hpp>

using namespace robofat2::pid;

pid::pid()
        : pid({1.0f, 0.0f, 0.0f}, nullptr, nullptr) {}

pid::pid(constants consts,
         std::unique_ptr<integrator> integral,
         std::unique_ptr<differentiator> derivative)
        : m_constants(consts),
          m_timer(),
          m_integrator(std::move(integral)),
          m_differentiator(std::move(derivative)) {}

pid::pid(float Kp, float Ki, float Kd, float iForgetRate, float iMaxMagnitude, float dFilterNow)
        : pid({Kp, Ki, Kd},
              std::unique_ptr<integrator>(
                      new rectangular_integrator(iForgetRate, iMaxMagnitude)),
              std::unique_ptr<differentiator>(
                      new ewma_differentiator(dFilterNow))) {}

float pid::process(float Error) {
    float DeltaTime = getDeltaTime();

    float Proportional = Error;

    float Integral = m_integrator
                     ? m_integrator->integrate(Error, DeltaTime)
                     : 0.0f;

    float Derivative = m_differentiator
                       ? m_differentiator->differentiate(Error, DeltaTime)
                       : 0.0f;

    return m_constants.Kp * Proportional +
           m_constants.Ki * Integral +
           m_constants.Kd * Derivative;
}

void pid::reset() {
    if (m_differentiator)
        m_differentiator->reset();

    if (m_integrator)
        m_integrator->reset();

    m_timer.reset_mark();
}

float pid::getDeltaTime() {
    float millis = m_timer.get_elapsed_since_mark().count() / 1000000.0f;
    m_timer.set_mark_to_now();
    if (millis < 1.0f) {
        millis = 1.0f;
    }
    return millis;
}

void pid::setIntegralFreeze(bool rly) {
    m_integrator->setFreeze(rly);
}
