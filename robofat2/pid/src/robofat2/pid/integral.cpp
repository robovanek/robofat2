#include <robofat2/pid/integrator.hpp>
#include <robofat2/utilities/numbers.hpp>

using namespace robofat2::pid;
using namespace robofat2::utilities;

rectangular_integrator::rectangular_integrator()
        : rectangular_integrator(1.0f, INFINITY) {}

rectangular_integrator::rectangular_integrator(float forgetRate,
                                               float maxMagnitude)
        : m_forgetRate(forgetRate),
          m_maximumMagnitude(maxMagnitude),
          m_currentValue(0.0f),
          m_isFrozen(false) {}

void rectangular_integrator::reset() {
    m_currentValue = 0.0f;
}

float rectangular_integrator::integrate(float error, float deltaTime) {
    if (!m_isFrozen) {
        m_currentValue *= m_forgetRate;
        m_currentValue += error * deltaTime;
        m_currentValue = limit(m_currentValue,
                               -m_maximumMagnitude,
                               +m_maximumMagnitude);
    }
    return m_currentValue;
}

void rectangular_integrator::setFreeze(bool really) {
    m_isFrozen = really;
}