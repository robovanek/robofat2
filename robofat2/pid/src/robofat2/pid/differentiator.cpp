#include <robofat2/pid/differentiator.hpp>
#include <cmath>
#include <cassert>

using namespace robofat2::pid;

unfiltered_differentiator::unfiltered_differentiator()
        : m_lastError(std::nanf("")) {}

void unfiltered_differentiator::reset() {
    m_lastError = std::nanf("");
}

float unfiltered_differentiator::differentiate(float error, float deltaTime) {
    float rate;

    if (std::isnan(m_lastError)) {
        rate = 0.0f;
    } else {
        rate = (error - m_lastError) / deltaTime;
    }

    m_lastError = error;
    return rate;
}

ewma_differentiator::ewma_differentiator(float currentWeight)
        : m_currentWeight(currentWeight),
          m_lastError(std::nanf("")),
          m_lastDerivative(std::nanf("")) {
    assert(currentWeight >= 0.0f);
    assert(currentWeight <= 1.0f);
}

ewma_differentiator::ewma_differentiator(float currentWeight, float oldWeight)
        : m_currentWeight(currentWeight / (currentWeight + oldWeight)),
          m_lastError(std::nanf("")),
          m_lastDerivative(std::nanf("")) {
    assert(currentWeight >= 0.0f);
    assert(currentWeight <= 1.0f);
}

void ewma_differentiator::reset() {
    m_lastError = std::nanf("");
    m_lastDerivative = std::nanf("");
}

float ewma_differentiator::differentiate(float Error, float DeltaTime) {
    if (std::isnan(m_lastError)) {
        m_lastError = Error;
        return 0.0f;
    }

    float DeltaError = Error - m_lastError;
    m_lastError = Error;

    float currentDerivative = DeltaError / DeltaTime;

    if (std::isnan(m_lastDerivative)) {
        m_lastDerivative = currentDerivative;
    } else {
        m_lastDerivative = currentDerivative * m_currentWeight +
                           m_lastDerivative * (1.0f - m_currentWeight);
    }
    return m_lastDerivative;
}