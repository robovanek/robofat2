/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PID data structures.
 */

#ifndef ROBOFAT2_PID_PARAMS_HPP
#define ROBOFAT2_PID_PARAMS_HPP

#include <cmath>

namespace robofat2 {
    namespace pid {
        /**
         * \brief PID constants
         *
         * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
         */
        struct constants {
            constants() = default;

            constants(float kp, float ki, float kd)
                    : Kp(kp), Ki(ki), Kd(kd) {}

            /**
             * \brief Proportional constant (Kp)
             */
            float Kp;
            /**
             * \brief Integral constant (Ki)
             */
            float Ki;
            /**
             * \brief Derivative constant (Kd)
             */
            float Kd;
        };
    }
}
#endif //ROBOFAT2_PID_PARAMS_HPP
