/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PID regulator.
 */

#ifndef ROBOFAT2_PID_HPP
#define ROBOFAT2_PID_HPP

#include <memory>
#include <robofat2/pid/constants.hpp>
#include <robofat2/pid/differentiator.hpp>
#include <robofat2/pid/integrator.hpp>
#include <robofat2/utilities/stopwatch.hpp>

namespace robofat2 {
    namespace pid {

        /**
         * \brief PID regulator
         * PID stands for Proportional, Integral and Derivative
         *
         * @author Jakub Vaněk <vanek.jakub4@seznam.cz>
         */
        class pid {
        public:
            /**
             * \brief Construct new empty PID instance.
             */
            pid();

            /**
             * \brief Construct new PID instance
             *
             * @param constants  PID constants
             * @param integral   Integrator implementation
             * @param dFilter    Differentiator implementation
             */
            pid(constants consts,
                std::unique_ptr<integrator> integral,
                std::unique_ptr<differentiator> derivative);

            /**
             * \brief Construct new PID instance with default implementations.
             *
             * @param Kp Proportional constant (Kp)
             * @param Ko Integral constant (Ki)
             * @param Kd Derivative constant (Kd)
             * @param iForgetRate Integral multiplier
             * @param iMaxMagnitude Integral maximum bounds
             * @param dFilterNow Weight of the current derivative value
             */
            pid(float Kp, float Ki, float Kd,
                float iForgetRate, float iMaxMagnitude,
                float dFilterNow);

            /**
             * \brief Process new error value
             *
             * The delta time is taken from the last call to this function.
             *
             * @param Error     New error value
             * @return          New correction
             */
            float process(float Error);

            /**
             * \brief Set whether integral should be frozen (i.e. restricted from growing).
             * @param rly True for freezing, false otherwise.
             */
            void setIntegralFreeze(bool rly);

            /**
             * Clear persistent parameters like derivative history and integral value.
             */
            void reset();

            /**
             * Get current PID parameter constants.
             * @return Structure containing PID parameters.
             */
            const constants &consts() const {
                return m_constants;
            }

            /**
             * Get current PID parameter constants.
             * @return Structure containing PID parameters.
             */
            constants &consts() {
                return m_constants;
            }

            /**
             * Get current integrator.
             * @return Reference to current integrator.
             */
            const integrator &integral() const {
                return *m_integrator;
            }

            /**
             * Get current integrator.
             * @return Reference to current integrator.
             */
            integrator &integral() {
                return *m_integrator;
            }

            /**
             * Get current differentiator.
             * @return Reference to current differentiator.
             */
            const differentiator &derivative() const {
                return *m_differentiator;
            }

            /**
             * Get current differentiator.
             * @return Reference to current differentiator.
             */
            differentiator &derivative() {
                return *m_differentiator;
            }

        private:
            /**
             * Get time elapsed since last iteration.
             * @return Elapsed time in milliseconds.
             */
            float getDeltaTime();

            constants m_constants;
            utilities::stopwatch m_timer;
            std::unique_ptr<integrator> m_integrator;
            std::unique_ptr<differentiator> m_differentiator;
        };
    }
}

#endif //ROBOFAT2_PID_HPP
