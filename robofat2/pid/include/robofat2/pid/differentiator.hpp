#ifndef ROBOFAT2_PID_DERIVATIVE_HPP
#define ROBOFAT2_PID_DERIVATIVE_HPP

namespace robofat2 {
    namespace pid {
        /**
         * Differentiator interface.
         *
         * This class converts a series of error values and time deltas to a series of signal derivatives.
         */
        class differentiator {
        public:
            /**
             * Destroy this differentiator.
             */
            virtual ~differentiator() = default;

            /**
             * Reset this differentiator to the initial state.
             */
            virtual void reset() = 0;

            /**
             * Do one round of differentiation.
             * @param error Current error.
             * @param deltaTime Current time delta.
             * @return Current derivative value.
             */
            virtual float differentiate(float error, float deltaTime) = 0;
        };

        /**
         * Simple differentiator without any filtering.
         */
        class unfiltered_differentiator : public differentiator {
        public:
            /**
             * Initialize this dumb differentiator.
             */
            explicit
            unfiltered_differentiator();

            /**
             * @inherit
             */
            void reset() final;

            /**
             * @inherit
             */
            float differentiate(float error, float deltaTime) final;

        private:
            /**
             * Last error value or NaN.
             */
            float m_lastError;
        };

        /**
         * Simple differentiator coupled with EWMA filtering.
         */
        class ewma_differentiator : public differentiator {
        public:
            /**
             * Initialize this EWMA differentiator.
             * @param currentWeight Mixing weight of the "new" derivative data (range 0.0f ~ 1.0f)
             */
            explicit
            ewma_differentiator(float currentWeight);

            /**
             * Initialize this EWMA differentiator.
             * @param currentWeight Mixing weight of the "new" derivative data (range 0.0f ~ INFINITY).
             * @param oldWeight Mixing weight of the "old" derivative data (range 0.0f ~ INFINITY).
             */
            ewma_differentiator(float currentWeight, float oldWeight);

            /**
             * @inherit
             */
            void reset() final;

            /**
             * @inherit
             */
            float differentiate(float error, float deltaTime) final;

        private:
            /**
             * Weight of the "new" derivative data, range 0.0f ~ 1.0f.
             *
             * Weight of the "old" data is determined as 1.0f - this value.
             */
            float m_currentWeight;
            /**
             * Last error value or NaN.
             */
            float m_lastError;
            /**
             * Last derivative value or NaN.
             */
            float m_lastDerivative;
        };
    }
}

#endif //ROBOFAT2_PID_DERIVATIVE_HPP
