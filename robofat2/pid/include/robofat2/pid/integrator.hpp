#ifndef ROBOFAT2_PID_INTEGRAL_HPP
#define ROBOFAT2_PID_INTEGRAL_HPP

namespace robofat2 {
    namespace pid {
        /**
         * Integrator interface.
         */
        class integrator {
        public:
            /**
             * Destroy this integrator.
             */
            virtual ~integrator() = default;

            /**
             * Reset this integrator to its initial state.
             */
            virtual void reset() = 0;

            /**
             * Do one round of integration.
             * @param error Current error value.
             * @param deltaTime Current time delta.
             * @return Current error integral value.
             */
            virtual float integrate(float error, float deltaTime) = 0;

            /**
             * Set whether integral should be frozen.
             * @param really True for yes, false for nope.
             */
            virtual void setFreeze(bool really) = 0;
        };

        /**
         * Simple (rectangular) integrator implementation.
         */
        class rectangular_integrator : public integrator {
        public:
            /**
             * Initialize new rectangular integrator without forgetting & limiting.
             */
            rectangular_integrator();

            /**
             * Initialize new rectangular integrator.
             * @param forgetRate Integrated error will be multiplied by this before each integration round.
             * @param maxMagnitude Integrated error will be limited by this after each integration round.
             */
            rectangular_integrator(float forgetRate, float maxMagnitude);

            /**
             * @inherit
             */
            void reset() final;

            /**
             * @inherit
             */
            float integrate(float error, float deltaTime) final;

            /**
             * @inherit
             */
            void setFreeze(bool really) final;

        private:
            /**
             * Pre-round integral multiplier.
             */
            float m_forgetRate;
            /**
             * Post-round integral limiter.
             */
            float m_maximumMagnitude;
            /**
             * Current error integral value.
             */
            float m_currentValue;
            /**
             * Whether the integral value should not change.
             */
            bool m_isFrozen;
        };
    }
}

#endif //ROBOFAT2_PID_INTEGRAL_HPP
