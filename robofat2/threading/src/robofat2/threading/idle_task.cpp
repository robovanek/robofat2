#include <thread>
#include <robofat2/threading/idle_task.hpp>

using namespace robofat2::threading;

idle_task::idle_task(long msec)
        : task("idle"),
          m_period(std::chrono::duration_cast<clock::duration>(
                  std::chrono::milliseconds(msec))),
          m_timer() {}

void idle_task::update() {
    // first loop is nonblocking
    if (m_timer.is_mark_empty()) {
        m_timer.set_mark_to_now();
        return;
    }
    // further loops block
    duration taken = m_timer.get_elapsed_since_mark();
    duration remaining = m_period - taken;
    if (remaining > clock::duration::zero()) {
        std::this_thread::sleep_for(remaining);
    }
    m_timer.set_mark_to_now();
}

void idle_task::skip_next_sleep() {
    m_timer.set_mark(m_timer.get_mark() - m_period);
}
