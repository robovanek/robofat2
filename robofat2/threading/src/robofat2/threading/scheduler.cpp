#include <robofat2/threading/scheduler.hpp>
#include <utility>
#include <csignal>

using namespace robofat2::threading;
using namespace robofat2::logging;

scheduler::scheduler()
        : scheduler(source::make_stderr("scheduler")) {}

scheduler::scheduler(logging::source logger)
        : task("scheduler"),
          m_loop(std::make_shared<std::atomic<bool>>(true)),
          m_logger(std::move(logger)) {}

void scheduler::enterLoop() {
    kernelSetup();

    while (m_loop->load()) {
        update();
    }
}

void scheduler::update() {
    for (task *pTask : m_tasks) {
        pTask->run();
    }
}

std::shared_ptr<std::atomic<bool>> global_stop;

static void global_sigint(int) {
    global_stop->store(false);
}

void scheduler::kernelSetup() {
    m_logger.debug("initializing realtime attributes");

    sched_param attrs = {};
    attrs.sched_priority = sched_get_priority_min(SCHED_FIFO);

    if (::sched_setscheduler(0, SCHED_RR, &attrs) == -1) {
        m_logger.log_errno(severity_error, "sched_setattr(SCHED_RR)", "<scheduler>");
    }

    global_stop = m_loop;

    struct sigaction sa = {nullptr};
    sa.sa_handler = &global_sigint;
    if (sigfillset(&sa.sa_mask) == -1) {
        m_logger.log_errno(severity_error, "sigfillset(...)", "<scheduler>");
    }
    if (sigaction(SIGINT, &sa, nullptr) == -1) {
        m_logger.log_errno(severity_error, "sigaction(...)", "<scheduler>");
    }
}
