
#include <string>
#include <utility>
#include <robofat2/threading/task.hpp>

using namespace robofat2::threading;

task::task(std::string name)
        : m_name(std::move(name)),
          m_run_after(clock::duration::zero()),
          m_run_timer() {}

const std::string &task::name() const {
    return m_name;
}

void task::force_wakeup() {
    m_run_timer.set_mark(m_run_timer.get_mark() - m_run_after);
}

void task::run() {
    if (m_run_after == clock::duration::zero()) {
        update();
    } else {
        if (m_run_timer.has_elapsed_since_mark(m_run_after) || m_run_timer.is_mark_empty()) {
            this->update();
            m_run_timer.set_mark_to_now();
        }

    }
}
