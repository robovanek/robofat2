#include <gtest/gtest.h>
#include <robofat2/threading/scheduler.hpp>
#include <csignal>

using namespace robofat2::threading;
using namespace robofat2::logging;

class stop_task : public task {
public:
    explicit
    stop_task(scheduler &sched)
            : task("stop task"),
              pSched(&sched) {}

    int updateCount = 0;
    int givenCount = 0;
    scheduler *const pSched;
protected:
    void update() override {
        updateCount++;
        if (updateCount >= givenCount) {
            pSched->setLooping(false);
        }
    }
};

class sigint_task : public task {
public:
    sigint_task() : task("sigint task") {}

    int updateCount = 0;
    int givenCount = 0;

protected:
    void update() override {
        updateCount++;
        if (updateCount >= givenCount) {
            std::raise(SIGINT);
        }
    }
};

class schedtest : public ::testing::Test {
protected:
    schedtest()
            : sched(source::make_discard()),
              tsk_stop(sched),
              tsk_sigint() {
        sched.append(tsk_stop);
        sched.append(tsk_sigint);
    }

    scheduler sched;
    stop_task tsk_stop;
    sigint_task tsk_sigint;

    int setLoopCount(int stop, int sigint) {
        tsk_stop.givenCount = stop;
        tsk_sigint.givenCount = sigint;
        return std::min(stop, sigint);
    }
};

TEST_F(schedtest, update_simple_test) {
    setLoopCount(1, 6);
    sched.enterLoop();
    EXPECT_GE(tsk_sigint.updateCount, 1);
    EXPECT_GE(tsk_stop.updateCount, 1);
}

TEST_F(schedtest, stop_test) {
    int expected = setLoopCount(3, 6);
    sched.enterLoop();
    EXPECT_EQ(tsk_sigint.updateCount, expected);
    EXPECT_EQ(tsk_stop.updateCount, expected);
}

TEST_F(schedtest, sigint_test) {
    int expected = setLoopCount(6, 3);
    sched.enterLoop();
    EXPECT_EQ(tsk_sigint.updateCount, expected);
    EXPECT_EQ(tsk_stop.updateCount, expected);
}
