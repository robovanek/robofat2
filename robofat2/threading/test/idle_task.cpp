#include <gtest/gtest.h>
#include <robofat2/threading/idle_task.hpp>

using namespace robofat2::threading;
using namespace robofat2::utilities;
using namespace std::chrono;

TEST(idle_task, just_wait) {
    const int interval = 50; // msec
    stopwatch msr;
    idle_task tsk(interval);

    msr.set_mark_to_now();
    tsk.update(); // first loop is nonblocking
    EXPECT_FALSE(msr.has_elapsed_since_mark(milliseconds(interval)));

    msr.set_mark_to_now();
    tsk.update();
    EXPECT_TRUE(msr.has_elapsed_since_mark(milliseconds(interval)));
}

TEST(idle_task, sleep_skip) {
    const int interval = 50; // msec
    stopwatch msr;
    idle_task tsk(interval);

    tsk.update(); // first loop is nonblocking
    msr.set_mark_to_now();
    tsk.skip_next_sleep();
    tsk.update(); // second sleep should be suppressed
    EXPECT_FALSE(msr.has_elapsed_since_mark(milliseconds(interval)));
}
