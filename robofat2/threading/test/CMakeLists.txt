cmake_minimum_required(VERSION 3.7)

project("RoboFAT2-Threading/Test" LANGUAGES CXX)

add_executable(robothread-test
        task.cpp
        scheduler.cpp
        idle_task.cpp
        )

target_link_libraries(robothread-test
        PRIVATE
        RoboFAT2::threading
        gtest_main
        )

set_target_properties(robothread-test PROPERTIES
        CXX_STANDARD 11
        CXX_STANDARD_REQUIRED YES
        CXX_EXTENSIONS NO)

include(GoogleTest)
gtest_add_tests(TARGET robothread-test)
