#include <gtest/gtest.h>
#include <robofat2/threading/task.hpp>
#include <thread>

using namespace robofat2::threading;
using namespace std::chrono;

class subtask : public task {
public:
    subtask() : task("name") {}

    explicit subtask(std::string name) : task(std::move(name)) {}

    void update() override {
        updateCount++;
    }

    void do_run() {
        return task::run();
    }

    int updateCount = 0;
};

TEST(task, name) {
    subtask tsk("name");
    EXPECT_EQ(tsk.name(), "name");
}

TEST(task, simple_update) {
    subtask tsk;
    tsk.update();
    tsk.update();
    EXPECT_EQ(tsk.updateCount, 2);
}

TEST(task, delta_set) {
    subtask tsk;

    tsk.set_run_period(milliseconds(50));
    EXPECT_EQ(tsk.get_run_period<milliseconds>().count(), 50);
}

TEST(task, nonzero_delta_sleep) {
    subtask tsk;
    tsk.set_run_period(milliseconds(50));

    EXPECT_EQ(tsk.updateCount, 0);
    tsk.do_run();
    EXPECT_EQ(tsk.updateCount, 1);
    tsk.do_run();
    EXPECT_EQ(tsk.updateCount, 1);
    std::this_thread::sleep_for(milliseconds(100));
    tsk.do_run();
    EXPECT_EQ(tsk.updateCount, 2);
}

TEST(task, force_run) {
    subtask tsk;
    tsk.set_run_period(milliseconds(50));
    EXPECT_EQ(tsk.updateCount, 0);
    tsk.do_run();
    tsk.force_wakeup();
    tsk.do_run();
    EXPECT_EQ(tsk.updateCount, 2);
}
