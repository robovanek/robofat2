#ifndef ROBOFAT_TASK_HPP
#define ROBOFAT_TASK_HPP

#include <string>
#include <chrono>
#include <robofat2/utilities/stopwatch.hpp>

namespace robofat2 {
    namespace threading {
        class scheduler;

        /**
         * Asynchronous thread of execution.
         *
         * This class should be subclassed to create a new thread.
         *
         * Scheduling is based on cooperative model.
         */
        class task {
        public:
            friend class scheduler;

            using clock = std::chrono::steady_clock;
            using duration = clock::duration;
            using time_point = clock::time_point;

        public:
            /**
             * Initialize a new task.
             * @param name Task name for easy identification.
             */
            explicit task(std::string name);

            /**
             * Destroy this task.
             */
            virtual ~task() = default;

            /**
             * Get the name of this task.
             * @return Name of this task.
             */
            const std::string &name() const;

            /**
             * Force running of this task even though it's repetition period hasn't yet elapsed.
             */
            void force_wakeup();

            /**
             * Get the repetition period of this task.
             * @tparam DurT Type of the std::chrono::duration.
             * @return Minimum time between task runs.
             */
            template<typename DurT>
            DurT get_run_period() const {
                return std::chrono::duration_cast<DurT>(m_run_after);
            }

            /**
             * Set the repetition period of this task.
             * @tparam DurT Type of the std::chrono::duration.
             * @param period Minimum time between task runs.
             */
            template<typename DurT>
            void set_run_period(DurT period) {
                m_run_after = std::chrono::duration_cast<decltype(m_run_after)>(period);
            }

        protected:
            /**
             * Mainloop - do a small part of current work each time.
             */
            virtual void update() = 0;

            /**
             * Run (or not) this task according to the repetition period.
             */
            void run();

        private:
            /**
             * Task name.
             */
            const std::string m_name;
            /**
             * Task repetition period.
             */
            clock::duration m_run_after;
            /**
             * Timer for tracking time elapsed since last run.
             */
            utilities::stopwatch m_run_timer;
        };
    }
}

#endif //ROBOFAT_TASK_HPP
