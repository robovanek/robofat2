#ifndef ROBOFAT_IDLE_TASK_HPP
#define ROBOFAT_IDLE_TASK_HPP

#include <chrono>
#include <robofat2/threading/task.hpp>
#include <robofat2/utilities/stopwatch.hpp>

namespace robofat2 {
    namespace threading {
        /**
         * Task for yielding inactive time to the operating system.
         */
        class idle_task : public task {
        public:
            /**
             * Initialize this idle task.
             * @param msec How long should one scheduler loop take (in milliseconds).
             */
            explicit idle_task(long msec);

            /**
             * Sleep until the desired total loop time is reached.
             */
            void update() override;

            /**
             * Force immediate rerun during the next round.
             */
            void skip_next_sleep();

        private:

            /**
             * Scheduler looping period.
             */
            duration m_period;

            /**
             * Timer for tracking elapsed time.
             */
            utilities::stopwatch m_timer;
        };
    }
}

#endif //ROBOFAT_IDLE_TASK_HPP
