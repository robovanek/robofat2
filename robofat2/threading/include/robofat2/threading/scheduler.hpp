#ifndef ROBOFAT_SCHEDULER_HPP
#define ROBOFAT_SCHEDULER_HPP

#include <list>
#include <atomic>
#include <robofat2/threading/task.hpp>
#include <robofat2/logging/source.hpp>

namespace robofat2 {
    namespace threading {
        /**
         * Cooperative scheduler.
         */
        class scheduler : public task {
        public:
            /**
             * Initialize this scheduler with default stderr logger.
             */
            scheduler();

            /**
             * Initialize this scheduler.
             * @param logger Logger for kernel initialization messages.
             */
            explicit scheduler(logging::source logger);

            /**
             * Append new task to the scheduler round.
             * @param ptr Task reference.
             */
            void append(task &ref) {
                m_tasks.push_front(&ref);
            }

            /**
             * Remove a task from the scheduler round.
             * @param ptr Task reference.
             */
            void remove(task &ref) {
                m_tasks.remove(&ref);
            }

            /**
             * Enable or disable scheduler mainloop.
             * @param rly True for looping, false for exit.
             */
            void setLooping(bool rly) {
                m_loop->store(rly);
            }

            /**
             * Enter the scheduler mainloop.
             */
            void enterLoop();

        protected:
            /**
             * Run one task round.
             */
            void update() override;

        private:
            /**
             * Setup kernel parameters (realtime scheduling and SIGINT stop handler).
             */
            void kernelSetup();

            /**
             * List of tasks to iterate over.
             */
            std::list<task *> m_tasks;
            /**
             * Whether
             */
            std::shared_ptr<std::atomic<bool>> m_loop;
            /**
             * Logger for kernel setup messages.
             */
            logging::source m_logger;
        };
    }
}

#endif //ROBOFAT_SCHEDULER_HPP
