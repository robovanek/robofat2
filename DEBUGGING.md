How to debug programs
=====================

1. mount second partition of ev3dev image as an EXT4 filesystem
  ```sh
  losetup -f /path/to/ev3dev.img
  losetup # look for ev3dev.img
  kpartx -a /dev/loopNN # partition from losetup
  mount /dev/mapper/loopNNp2 /mnt
  ```
2. copy it somewhere to your local drive (that place will be called 'sysroot')
  ```sh
  rsync -aHAX /mnt /sysroot
  ```
3. unmount the second partition
  ```sh
  umount /dev/mapper/loopNNp2
  kpartx -d /dev/loopNN
  losetup -d /dev/loopNN
  ```
4. install qemu-user-static on host and copy qemu-arm-static to <sysroot>/usr/bin
  ```sh
  apt install qemu-user-static
  cp /usr/bin/qemu-arm-static /sysroot/usr/bin
  ```
5. bind-mount necessary partitions:
  ```sh
  mount --bind  /sysroot /sysroot
  mount --rbind /dev     /sysroot/dev
  mount --rbind /sys     /sysroot/sys
  mount --rbind /proc    /sysroot/proc
  mount --rbind /run     /sysroot/run
  mkdir -p /sysroot/home/compiler/program
  mount --bind  /path/to/sources/prg /home/compiler/program
  ```
6. enter chroot (this should be done from root shell, not via sudo)
  ```sh
  chroot /sysroot
  ```
7. install necessary packages into chroot
  ```sh
  echo "deb http://deb.debian.org/debian-debug/ stretch-debug main" | tee /etc/apt/sources.list.d/debug.list
  apt update
  apt full-upgrade
  apt install libc6-dbg libstdc++6-6-dbg libudev1 libudev1-dbgsym libncurses5-dev gdb
  ```
8. start debugging
  ```sh
  cd /home/compiler/program/build
  gdb -ex 'set sysroot /' -ex 'target remote 10.42.0.76:6666' -ex 'cont' ducttape/ducttape.dbg
  ```
9. leave chroot
  ```sh
  exit
  ```
10. unmount partitions
  ```sh
  mount --make-rslave /sysroot
  umount /sysroot
  mount --make-rslave /sysroot/dev
  mount --make-rslave /sysroot/sys
  mount --make-rslave /sysroot/proc
  mount --make-rslave /sysroot/run
  umount -R /sysroot/dev /sysroot/sys /sysroot/proc /sysroot/run
  umount /sysroot/home/compiler/program
  ```
