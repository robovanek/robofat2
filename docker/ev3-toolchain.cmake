# Specify OS
SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_VERSION 4.14.117)
SET(CMAKE_SYSTEM_PROCESSOR arm)

# Specify compiler
SET(CMAKE_C_COMPILER arm-linux-gnueabi-gcc)
SET(CMAKE_CXX_COMPILER arm-linux-gnueabi-g++)

# Specify flags
SET(CMAKE_C_FLAGS   "-march=armv5t -mtune=arm926ej-s" CACHE STRING "C   compiler flags" FORCE)
SET(CMAKE_CXX_FLAGS "-march=armv5t -mtune=arm926ej-s" CACHE STRING "CXX compiler flags" FORCE)
