#!/bin/bash
DIR="$( cd "$( dirname $( readlink -f "${BASH_SOURCE[0]}" ) )/.." >/dev/null 2>&1 && pwd )"
IMAGE="registry.gitlab.com/robovanek/robofat2/builder-arm:latest"

if [[ "$1" = "release" ]]; then
    BUILD_TYPE="Release"
elif [[ "$1" = "debug" ]]; then
    BUILD_TYPE="Debug"
else
    BUILD_TYPE="RelWithDebInfo"
fi

shift
docker run --rm -it -v "$DIR:/home/compiler/program" -e BUILD_TYPE=$BUILD_TYPE "$IMAGE" /home/compiler/cmake.sh "$@"
