#!/bin/bash
DIR="$( cd "$( dirname $( readlink -f "${BASH_SOURCE[0]}" ) )/.." >/dev/null 2>&1 && pwd )"
IMAGE="registry.gitlab.com/robovanek/robofat2/builder-arm:latest"

docker run --rm -it -v "$DIR:/home/compiler/program" "$IMAGE" /home/compiler/build.sh -j2 test "$@"
